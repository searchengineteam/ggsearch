<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script src="js/highlight.js"></script>
    <script src="js/QueryController.js"></script>
    <link type="text/css" href="css/main.css" rel="stylesheet"/>
    <title>GGSearch</title>
    <script>
        $(document).ready(function () {
            QueryController.init("#search", "#submit", "#gglogo", "#result-wrapper");
        })
    </script>
</head>
<body>
<div id="content">

    <div id="search-wrapper" class="ggclear">
        <h1 id="gglogo">GGSearch</h1>
        <form id="searchbox" class="ggclear" action="">
            <input id="search" type="text" placeholder="Type here">
            <input id="submit" type="submit" value="Search">
            <input type="radio" name="search-type" value="cos"> CosSim
            <input type="radio" name="search-type" value="prod"> InnerProduct
            <input type="radio" name="search-type" value="pagerank">PageRank
        </form>
    </div>
    <div id="result-wrapper" class="ggclear">

    </div>
</div>
</body>
</html>
