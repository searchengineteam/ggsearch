var QueryController = {
    isCompact: false,
    onSearchClickListeners: [],
    init: function (inputId, btnId, logoId, resultId) {
        this.input = $(inputId);
        this.btn = $(btnId);
        this.logo = $(logoId);
        this.result = $(resultId);
        var self = this;
        this.btn.click(function (event) {
            event.preventDefault();
            self.onSearchClick();
        })
    },
    onSearchClick: function () {
        this.queryStr = this.getQuery();
        var self = this;
        $.get("q", {query: JSON.stringify(this.queryStr)}).done(function (data) {
            self.rVal = $.parseJSON(data);
            self.result.empty();
            self.renderResult();
        });
        this.toCompact();
    },
    getQuery: function () {
        var type = $('input:radio[name=search-type]:checked').val();
        var query = this.input.val();
        var rval = {
            "query":query,
            "type":type
        };
        return rval;
    },
    isCompact: false,
    toCompact: function () {
        if (!this.isCompact) {
            this.logo.css("font-size", "20px");
            this.logo.css("float", "left");
            this.logo.css("margin-top", "0");
            this.logo.css("margin-bottom", "0");
            $("#searchbox").css("float", "right");
            this.isCompact = true;
        }

    },
    renderItem: function (title, text, query, url, score) {
        var dom = $('<div class="result-item"></div>');
        var titleDom = $('<h2 class="item-title"></h2>');
        var titleHref = $('<a></a>');
        titleHref.attr('href', url);
        titleHref.append(title);
        titleDom.append(titleHref);
        var metaDom = $('<p class="meta"></p>');
        metaDom.append(score);
        var textDom = $('<p></p>');
        textDom.append(text);
        query = query.query.split(' ');
        //Do highlighting
        for (var i = 0; i < query.length; i++) {
            query[i] = query[i].replace(/["0-9.:\-]/gi, '');
            console.log(query[i]);
            titleDom.highlight(query[i]);
            titleDom.highlight(query[i].toLowerCase());
            textDom.highlight(query[i]);
            textDom.highlight(query[i].toLowerCase());
        }
        dom.append(titleDom);
        dom.append(metaDom);
        dom.append(textDom);
        return dom;
    },
    renderResult: function () {
        var result = this.rVal;
        for(var i = 0; i < result.length; i++) {
            dom = this.renderItem(result[i].title, result[i].text,
                this.getQuery(), result[i].url, result[i].score);
            this.result.append(dom);
        }
    }
};