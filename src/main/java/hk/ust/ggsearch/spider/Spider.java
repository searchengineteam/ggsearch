package hk.ust.ggsearch.spider;

import hk.ust.ggsearch.utils.StopStem;
import hk.ust.ggsearch.domain.HTMLPage;
import hk.ust.ggsearch.index.Index;

import java.net.MalformedURLException;
import java.util.*;

public class Spider {
    //static Index idx = Index.getInstance(); //No need currently
    Index idx = Index.getInstance();
    String mRootURL;
    Queue<String> mBFSQueue = new LinkedList<String>();
    HashSet<String> mExisted = new HashSet<String>();
    StopStem stopStem = new StopStem("stopwords.txt");
    int pageCnt = 0;  //Number of pages graped
    int maxPageNum;  //The maximum number of pages to be indexed


    /**
     * Constructor
     *
     * @param rootURL
     * @param maxPageNum
     */
    public Spider(String rootURL, int maxPageNum) {
        this.mRootURL = rootURL;
        this.maxPageNum = maxPageNum;
        initialize();
    }

    /**
     * Initialize the status
     */
    private void initialize() {
        mExisted.add(mRootURL);
        mBFSQueue.add(mRootURL);
    }

    public boolean hasNext() {
        return pageCnt <= maxPageNum && !mBFSQueue.isEmpty();
    }

    public HTMLPage nextPage() {
        try {
            HTMLPage rVal = new HTMLPage();
            String curURLStr = mBFSQueue.poll();
            rVal.url = curURLStr;
            String title = SpiderUtils.getPageTitle(curURLStr);
            rVal.rawTitle = title;
            List<String> childrenURL = SpiderUtils.getChilrenURLs(curURLStr);
            //Insert children to the queue
            for (String child : childrenURL) {
                //Avoid cycle
                if (!mExisted.contains(child)) {
                    mExisted.add(child);
                    mBFSQueue.add(child);
                }

            }

            List<String> bodyTermsLst = SpiderUtils.getBodyTerms(curURLStr);
            bodyTermsLst.remove(title);
            List<String> stemmedBodyLst = new ArrayList<String>();
            for (String node : bodyTermsLst) {
                //Tokenize
                String[] terms = node.split(" ");
                //skip stop words
                for (String term : terms) {
                    if (!stopStem.isStopWord(term)) {
                        stemmedBodyLst.add(stopStem.stem(term));
                    }
                }
            }
            rVal.body = stemmedBodyLst;
            rVal.children = childrenURL;
            rVal.modifiedDate = SpiderUtils.lastModify(curURLStr);
            rVal.rawHtml = SpiderUtils.getPage(curURLStr);

            String[] titleTerms = title.split(" ");
            rVal.title = Arrays.asList(titleTerms);
            pageCnt++;
            return rVal;
        } catch (MalformedURLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return null;
    }
}