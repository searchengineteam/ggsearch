package hk.ust.ggsearch.spider;

import org.htmlparser.beans.LinkBean;
import org.htmlparser.beans.StringBean;
import org.htmlparser.util.ParserException;
import org.joda.time.DateTime;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: gary_li
 * Date: 10/22/13
 * Time: 10:14 PM
 * To change this template use File | Settings | File Templates.
 */
public class SpiderUtils {
    /**
     * Extract words in url and return them
     * Use String tokenizer to tokenize the result from StringBean
     *
     * @return tokens of web page with given URL
     * @throws ParserException
     */
    public static ArrayList<String> extractWords(String url) throws ParserException {
        StringBean sb;
        ArrayList<String> tokens = new ArrayList<String>();
        boolean links = false;
        sb = new StringBean();
        sb.setLinks(links);
        sb.setURL(url);
        String temp = sb.getStrings();
        StringTokenizer st = new StringTokenizer(temp, "\n");
        while (st.hasMoreTokens()) {
            tokens.add(st.nextToken());
        }
        return tokens;
    }

    public static ArrayList<String> extractLinks(String targetURL) throws ParserException {
        ArrayList<String> rURLs = new ArrayList<String>();
        LinkBean lb = new LinkBean();
        lb.setURL(targetURL);
        URL[] URLs = lb.getLinks();
        for (URL url : URLs) {
            rURLs.add(url.toString());
        }
        return rURLs;
    }

    /**
     * Get the maxPageNum modification date
     *
     * @param urlStr
     * @return
     */
    public static DateTime lastModify(String urlStr) throws MalformedURLException {
        URL url = new URL(urlStr);
        long time = 0;
        try {
            //url = new URL(mUrl);
            URLConnection connection = url.openConnection();
            connection.connect();
            time = connection.getLastModified();
            // System.out.println(time);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        DateTime date = new DateTime(1970, 1, 1, 0, 0, 0, 0);
        int second = (int) (long) (time / 1000);
        date = date.plusSeconds(second);
        //System.out.println(date);
        return date;

    }

    public static List<String> getChilrenURLs(String mURL) throws ParserException {
        List<String> links = new ArrayList<String>();
        String ti;
        links = extractLinks(mURL);
        //mExisted.add(head[]);
        return links;
    }

    /**
     * @param mURL
     * @return
     */
    public static List<String> getBodyTerms(String mURL) throws ParserException {

        List<String> words = new ArrayList<String>();
        words = extractWords(mURL);
        //mExisted.add(head[]);
        return words;
    }

    public static String getPageTitle(String urlStr) throws Exception {
        Document doc = Jsoup.connect(urlStr).get();
        Elements titleEle = doc.select("title");
        return titleEle.html();
    }

    public static String getPage(String urlStr) throws Exception {
        URL url = new URL(urlStr);
        BufferedReader in = new BufferedReader(
                new InputStreamReader(url.openStream()));
        String inputLine;

        String html = "";
        try {
            while (!((inputLine = in.readLine()) == null)) {
                html = html + inputLine;
            }
            in.close();
            //System.out.println(html);
        } catch (Exception e) {
        }
        return html;
    }
}
