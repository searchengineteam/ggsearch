package hk.ust.ggsearch.domain;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.mapdb.Serializer;

/**
 * Serializers
 */
public class GGSerializer {
    public static Serializer<ForwardIndexItem> forwardIndexSerializer  = new ForwardIndexSerializer ();
    public static Serializer<DocCacheIndexItem> docCacheIndexSerializer = new DocCacheIndexSerializer();
    public static Serializer<TermInfoIndexItem> termInfoIndexItemSerializer = new TermInfoIndexItemSerializer();
    public static Serializer<DocInfoIndexItem> docInfoIndexItemSerializer = new DocInfoIndexItemSerializer();
    public static Serializer<DocTrashIndexItem> docTrashIndexItemSerializer = new DocTrashIndexItemSerializer();
    public static Serializer<LinkBasedInfoIndexItem> linkBasedInfoIndexItemSerializer = 
            new LinkBasedInfoIndexItemSerializer();
    public static Serializer<PostingItem> postingItemSerializer = new PostingItemSerializer();
    
    public static class ForwardIndexSerializer implements Serializer<ForwardIndexItem>, Serializable {
        public ForwardIndexItem deserialize(DataInput in, int available)
                throws IOException {
            int tfmax = in.readInt();
            double l2Norm = in.readDouble();
            int size = in.readInt();
            ArrayList<Long> terms = new ArrayList<Long>();
            for (int i = 0; i < size; i++) {
                terms.add(in.readLong());
            }
            return new ForwardIndexItem(tfmax, terms, l2Norm);
        }

        public void serialize(DataOutput out, ForwardIndexItem value)
                throws IOException {
            out.writeInt(value.tfMax);
            out.writeDouble(value.l2Norm);
            int size = value.terms.size();
            out.writeInt(size);
            for (int i = 0; i < size; i++) {
                out.writeLong(value.terms.get(i));
            }
        }
    }
    
    public static class DocCacheIndexSerializer implements Serializer<DocCacheIndexItem>, Serializable {
        public DocCacheIndexItem deserialize(DataInput in, int available)
                throws IOException {
            return new DocCacheIndexItem(in.readUTF(), in.readUTF(), in.readDouble());
        }

        public void serialize(DataOutput out, DocCacheIndexItem value)
                throws IOException {
            out.writeUTF(value.title);
            out.writeUTF(value.htmlText);
            out.writeDouble(value.pageSize);
        }
    }
    
    public static class TermInfoIndexItemSerializer implements Serializer<TermInfoIndexItem>, Serializable {
        public TermInfoIndexItem deserialize(DataInput in, int available)
                throws IOException {
            return new TermInfoIndexItem(in.readLong(), in.readLong());
        }

        public void serialize(DataOutput out, TermInfoIndexItem value)
                throws IOException {
            out.writeLong(value.titleDf);
            out.writeLong(value.bodyDf);
        }
    }
    
    public static class DocInfoIndexItemSerializer implements Serializer<DocInfoIndexItem>, Serializable {
        public DocInfoIndexItem deserialize(DataInput in, int available)
                throws IOException {
            long millis = in.readLong();
            DateTime lastModifiedDate = null;
            if (millis != -1) {
                DateTimeZone dtz = DateTimeZone.forID(in.readUTF());
                lastModifiedDate = new DateTime(millis, dtz);
            }
            return new DocInfoIndexItem(lastModifiedDate);
        }

        public void serialize(DataOutput out, DocInfoIndexItem value)
                throws IOException {
            if (value.lastModifiedDate == null) {
                out.writeLong(-1);                
            } else {
                out.writeLong(value.lastModifiedDate.getMillis());
                out.writeUTF(value.lastModifiedDate.getZone().getID());
            }
        }
    }
    
    public static class DocTrashIndexItemSerializer implements Serializer<DocTrashIndexItem>, Serializable {
        public DocTrashIndexItem deserialize(DataInput in, int available)
                throws IOException {
            return new DocTrashIndexItem(in.readLong(), in.readLong());
        }

        public void serialize(DataOutput out, DocTrashIndexItem value)
                throws IOException {
            out.writeLong(value.oldDocId);
            out.writeLong(value.newDocId);
        }
    }
    
    public static class LinkBasedInfoIndexItemSerializer implements Serializer<LinkBasedInfoIndexItem>, Serializable {
        public LinkBasedInfoIndexItem deserialize(DataInput in, int available)
                throws IOException {
            return new LinkBasedInfoIndexItem(in.readDouble());
        }

        public void serialize(DataOutput out, LinkBasedInfoIndexItem value)
                throws IOException {
            out.writeDouble(value.pageRank);
        }
    }
    
    public static class PostingItemSerializer implements Serializer<PostingItem>, Serializable {
        public PostingItem deserialize(DataInput in, int available)
                throws IOException {
            int freq = in.readInt();
            int positionListSize = in.readInt();
            ArrayList<Integer> positions = new ArrayList<Integer>();
            for (int i = 0; i < positionListSize; i++) {
                int position = in.readInt();
                positions.add(position);
            }
            return new PostingItem(freq, positions);
        }

        public void serialize(DataOutput out, PostingItem value)
                throws IOException {
            out.writeInt(value.freq);
            out.writeInt(value.positions.size());
            for (int i = 0; i < value.positions.size(); i++) {
                out.writeInt(value.positions.get(i));
            }
        }
    }
}
