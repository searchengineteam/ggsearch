package hk.ust.ggsearch.domain;

public class LinkBasedInfoIndexItem {
    public double pageRank;
    
    public LinkBasedInfoIndexItem() {
        this.pageRank = 1;
    }
    
    public LinkBasedInfoIndexItem(double pageRank) {
        this.pageRank = pageRank;
    }
}
