package hk.ust.ggsearch.domain;


/**
 * [docId] HTMLPage
 */
public class DocCacheIndexItem {

    public String title;
    public String htmlText;
    public double pageSize;

    public DocCacheIndexItem(String title, String htmlText, double pageSize) {
        this.title = title;
        this.htmlText = htmlText;
        this.pageSize = pageSize;
    }

    @Override
    public String toString() {
        return this.title + "\n" + this.htmlText;
    }
}
