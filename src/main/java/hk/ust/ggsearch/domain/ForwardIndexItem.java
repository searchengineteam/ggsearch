package hk.ust.ggsearch.domain;

import java.util.ArrayList;
import java.util.List;

public class ForwardIndexItem {
    public List<Long> terms; //termIds
    public int tfMax;
    public double l2Norm;
    
    public ForwardIndexItem(int tfmax, List<Long> termList, double l2Norm) {
        this.tfMax = tfmax;
        this.terms = new ArrayList<Long>(termList);
        this.l2Norm = l2Norm;
    }
    
    @Override
    public String toString() {
        String rVal = "[";
        for(Long wId: terms) {
            rVal = rVal+ wId.toString()+',';
        }
        rVal = rVal+"]";
        return rVal;
    }
}
