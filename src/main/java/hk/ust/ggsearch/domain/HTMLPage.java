package hk.ust.ggsearch.domain;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;

/**
 * Created with IntelliJ IDEA.
 * User: gary_li
 * Date: 10/1/13
 * Time: 9:18 PM
 * To change this template use File | Settings | File Templates.
 */
public class HTMLPage {
    public String url;
    public DateTime modifiedDate;
    public List<String> children;
    public List<String> title;
    public List<String> body;
    public String rawHtml;
    public String rawTitle;
    @Override
    public String toString() {
        String rStr = "";
        rStr = rStr + StringUtils.join("title:" + title, " ") + "\n";
        rStr = rStr + modifiedDate.toString() + "\n";
        rStr = rStr + StringUtils.join(children, "\n") + "\n";
        rStr = rStr + StringUtils.join(body, " ") + "\n";
        return rStr;
    }
}
