package hk.ust.ggsearch.domain;

import java.util.ArrayList;
import java.util.List;

public class PostingItem {
    public int freq;
    public List<Integer> positions;

    public PostingItem(int freq, ArrayList<Integer> positions) {
        this.freq = freq;
        this.positions = new ArrayList<Integer>(positions);
    }

    @Override
    public String toString() {
        String rVal = String.format("freq:%d, pos:[",  freq);
        for (Integer pos : positions) {
            rVal = rVal + pos.toString() + ",";
        }
        rVal = rVal + "]\n";
        return rVal;
    }
}
