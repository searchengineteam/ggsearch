package hk.ust.ggsearch.domain;

public class TermInfoIndexItem {
    public long titleDf;
    public long bodyDf;
    
    public TermInfoIndexItem(long titleDf, long bodyDf) {
        this.titleDf = titleDf;
        this.bodyDf = bodyDf;
    }
}
