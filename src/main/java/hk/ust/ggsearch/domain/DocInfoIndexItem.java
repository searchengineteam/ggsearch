package hk.ust.ggsearch.domain;

import org.joda.time.DateTime;

public class DocInfoIndexItem {
    public DateTime lastModifiedDate;
    
    public DocInfoIndexItem(DateTime lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }
}
