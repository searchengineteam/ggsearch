package hk.ust.ggsearch.domain;


public class DocTrashIndexItem {
    public long oldDocId;
    public long newDocId;

    public DocTrashIndexItem(long oldDocId, long newDocId) {
        this.oldDocId = oldDocId;
        this.newDocId = newDocId;
    }
}
