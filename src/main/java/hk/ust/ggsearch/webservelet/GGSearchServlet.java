package hk.ust.ggsearch.webservelet;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import hk.ust.ggsearch.domain.DocCacheIndexItem;
import hk.ust.ggsearch.index.Index;
import hk.ust.ggsearch.query.PageRankRetriever;
import hk.ust.ggsearch.query.QueryParser;
import hk.ust.ggsearch.query.SimilarityCalculator;
import hk.ust.ggsearch.query.WeightedToken;
import org.jsoup.Jsoup;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class GGSearchServlet extends HttpServlet {
    static Index idx = Index.getInstance();
    static Gson gson = new Gson();
    static final int summaryLength = 400;

    String mQuery = "";
    String mType = "";

    @Override
    public void doGet(HttpServletRequest req,
                      HttpServletResponse res) throws ServletException, IOException {
        System.out.println("Working Directory = " +
                System.getProperty("user.dir"));
        String jsonData = req.getParameter("query");
        System.out.println(jsonData);
        JsonObject jobject = (new JsonParser()).parse(jsonData).getAsJsonObject();
        mQuery = jobject.get("query").toString();
        mQuery = mQuery.substring(1, mQuery.length() - 1).replace("\\", "");
        mType = jobject.get("type").toString().replace("\"", "");
        System.out.println(mQuery);
        System.out.println(mType);
        QueryParser parser = new QueryParser();
        List<WeightedToken> tokens = parser.process(mQuery);
        for (WeightedToken token : tokens) {
            System.out.println(token.toString());
        }
        Map<Long, Double> retrievedDocs = null;
        if (mType.equals("cos")) {
            retrievedDocs = SimilarityCalculator.getSortedCosineSimilarityTable(tokens);
        } else if (mType.equals("prod")) {
            retrievedDocs = SimilarityCalculator.getSortedInnerProductSimilarityTable(tokens);
        } else if (mType.equals("pagerank")) {
            retrievedDocs = PageRankRetriever.getSortedPageRnkTable(tokens);
        } else {
            return;
        }

        //For testing
        String json = genRJson(retrievedDocs);
        res.getWriter().write(json);
    }

    public String genRJson(Map<Long, Double> docs) {
        List<JSONDocItem> items = new ArrayList<JSONDocItem>();
        for (Map.Entry<Long, Double> entry: docs.entrySet()) {
            items.add(getJsonItemFromDocID(entry.getKey(), entry.getValue()));
        }
        return gson.toJson(items.toArray());

    }

    private JSONDocItem getJsonItemFromDocID(long docId, double score) {
        JSONDocItem newItem = new JSONDocItem();
        newItem.url = idx.getDocMap().getURL(docId);
        DocCacheIndexItem cacheItem = idx.getDocCache().getDocCacheIndexItem(docId);
        //System.out.println(cacheItem.toString());
        newItem.text = getSummary(Jsoup.parse(cacheItem.htmlText).text(), mQuery);
        newItem.title = cacheItem.title;
        newItem.score = score;
        return newItem;
    }

    public static class JSONDocItem {
        public String url;
        public String title;
        public String text;
        public double score;
    }

    public static String getSummary(String textOri, String queryOri) {
        String text = textOri.toLowerCase();
        String query = queryOri.replaceAll("[\"0-9\\.\\-:]", "").toLowerCase();

        String[] tokens = query.split(" ");
        int textLength = text.length();
        int maxIdx = -1;
        int minIdx = -1;
        for (String token : tokens) {
            int idx = text.indexOf(token);
            if (maxIdx < 0 || maxIdx < idx) {
                maxIdx = idx;
            }
            if (minIdx < 0 || minIdx > idx) {
                minIdx = idx;
            }
        }
        int endLength = summaryLength;
        if (minIdx - summaryLength < 0) {
            minIdx = 0;
            endLength += summaryLength - minIdx;
        } else {
            minIdx -= summaryLength;
        }

        int endPos = maxIdx + endLength;
        if (endPos >= textLength) {
            endPos = textLength - 1;

        }
        //System.out.println("" + minIdx +":" + endPos);
        return text.substring(minIdx, endPos);
    }
}
