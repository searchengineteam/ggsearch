package hk.ust.ggsearch.query;

import hk.ust.ggsearch.domain.PostingItem;
import hk.ust.ggsearch.index.Index;
import hk.ust.ggsearch.utils.Pair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NavigableSet;

import org.mapdb.Fun;


public class PartialScoreCalculator {
    private static Index idx = Index.getInstance();

    /**
     * Get a list of (doc, partialScore) tuples
     *
     * @param phase the phase to process
     * @return list of partial scores
     */
    public static List<Pair<Long, Pair<Double, Double>>> getPartialScoresOfPhase(List<WeightedToken> phase) {
        List<String> terms = new ArrayList<String>();
        System.out.println("Processing phase");
        for (WeightedToken term : phase) {
            System.out.println(term.term);
            terms.add(term.term);
        }
        List<Long> termIds = idx.getTermMap().getTermIds(terms);
        List<NavigableSet<Fun.Tuple2<Long, Long>>> bodyPostLstKeys =
                new ArrayList<NavigableSet<Fun.Tuple2<Long, Long>>>();
        List<NavigableSet<Fun.Tuple2<Long, Long>>> titlePostLstKeys =
                new ArrayList<NavigableSet<Fun.Tuple2<Long, Long>>>();
        for (long termId : termIds) {
            bodyPostLstKeys.add(
                    idx.getInvertedIndices().getBodyPostingItemKeys(termId));
            titlePostLstKeys.add(
                    idx.getInvertedIndices().getTitlePostingItemKeys(termId));
        }
        List<List<PostingItemAdapter>> bodyAdaptedItems = keySetsToAdaptedItems(bodyPostLstKeys,
                new ItemGetters.BodyIIdxItemGetter());
        List<List<PostingItemAdapter>> titleAdaptedItems = keySetsToAdaptedItems(titlePostLstKeys,
                new ItemGetters.TitleIIdxItemGetter());

        List<PhasePostingItem> bodyPhasePostingItemLst =
                findPhasePostingItems(bodyAdaptedItems);
        List<PhasePostingItem> titlePhasePostingItemLst =
                findPhasePostingItems(titleAdaptedItems);
        List<Pair<Long, Double>> bodyTfidf =
                calPhaseTfidf(bodyPhasePostingItemLst, new ItemGetters.BodyFIdxItemGetter());
        List<Pair<Long, Double>> titleTfidf =
                calPhaseTfidf(titlePhasePostingItemLst, new ItemGetters.TitleFIdxItemGetter());
        return tfidfToPartialScore(bodyTfidf, titleTfidf);
    }

    public static List<Pair<Long, Pair<Double, Double>>> getPartialScoreOfTerm(String term) {
        long termId = idx.getTermMap().getTermId(term);
        NavigableSet<Fun.Tuple2<Long, Long>> bodyPostingItems
                = idx.getInvertedIndices().getBodyPostingItemKeys(termId);
        NavigableSet<Fun.Tuple2<Long, Long>> titlePostingItems
                = idx.getInvertedIndices().getTitlePostingItemKeys(termId);
        List<Pair<Long, Double>> bodyTfidf = new ArrayList<Pair<Long, Double>>();
        List<Pair<Long, Double>> titleTfidf = new ArrayList<Pair<Long, Double>>();

        int bodyDf = bodyPostingItems.size();
        for (Fun.Tuple2<Long, Long> itemKey : bodyPostingItems) {
            PostingItem item = idx.getInvertedIndices().getBodyPostingItem(itemKey.a, itemKey.b);
            int tf = item.freq;
            double idf = Math.log(idx.getDocMap().getNumOfDoc() / (double) bodyDf) / Math.log(2);
            bodyTfidf.add(new Pair<Long, Double>(itemKey.b, tf * idf));
        }
        int titleDf = titlePostingItems.size();
        for (Fun.Tuple2<Long, Long> itemKey : titlePostingItems) {
            PostingItem item = idx.getInvertedIndices().getTitlePostingItem(itemKey.a, itemKey.b);
            int tf = item.freq;
            double idf = Math.log(idx.getDocMap().getNumOfDoc() / (double) titleDf) / Math.log(2);
            titleTfidf.add(new Pair<Long, Double>(itemKey.b, tf * idf));
        }
        return tfidfToPartialScore(bodyTfidf, titleTfidf);
    }

    private static List<Pair<Long, Pair<Double, Double>>> tfidfToPartialScore(
            List<Pair<Long, Double>> body, List<Pair<Long, Double>> title) {
        List<Pair<Long, Pair<Double, Double>>> rVal = new ArrayList<Pair<Long, Pair<Double, Double>>>();
        HashMap<Long, Pair<Double, Double>> tfidfMap = new HashMap<Long, Pair<Double, Double>>();
        for (Pair<Long, Double> item : body) {
            int tfMax = idx.getForwardIndices().getBodyForwardIndexItem(item.first()).tfMax;
            tfidfMap.put(item.first(),
                    new Pair<Double, Double>(0D, item.second() / tfMax)); //doc, bodyScore * bodyWeight
        }

        for (Pair<Long, Double> item : title) {
            int tfMax = idx.getForwardIndices().getTitleForwardIndexItem(item.first()).tfMax;
            double titleScore = item.second() / tfMax;
            if (tfidfMap.containsKey(item.first())) {
                Pair<Double, Double> partialScores = tfidfMap.get(item.first());
                double bodyScore = partialScores.second();
                tfidfMap.put(item.first(), new Pair<Double, Double>(titleScore, bodyScore));
            } else {
                tfidfMap.put(item.first(), new Pair<Double, Double>(titleScore, 0D));
            }
        }
        Iterator<Map.Entry<Long, Pair<Double, Double>>> iter = tfidfMap.entrySet().iterator();
        while (iter.hasNext()) {
            Map.Entry<Long, Pair<Double, Double>> entry = iter.next();
            rVal.add(new Pair<Long, Pair<Double, Double>>(entry.getKey(), entry.getValue()));
        }
        return rVal;
    }

    /**
     * Format conversion, for looser coupling. Convert keyset to Adapted item list
     *
     * @param keySets
     * @return Every list inside a list includes documents of a single term
     */
    private static List<List<PostingItemAdapter>> keySetsToAdaptedItems(
            List<NavigableSet<Fun.Tuple2<Long, Long>>> keySets, ItemGetters.IIdxItemGetter itemGetter) {
        List<List<PostingItemAdapter>> rVal =
                new ArrayList<List<PostingItemAdapter>>();

        for (NavigableSet<Fun.Tuple2<Long, Long>> keySet : keySets) {
            List<PostingItemAdapter> postingItemLst = new ArrayList<PostingItemAdapter>();
            for (Fun.Tuple2<Long, Long> key : keySet) { //(termId, docId) of a term
                PostingItem postingItem = itemGetter.getItem(idx, key);
                PostingItemAdapter newAdaptedItem = new PostingItemAdapter(postingItem, key.b);
                postingItemLst.add(newAdaptedItem);
            }
            rVal.add(postingItemLst);
        }
        return rVal;
    }

    /**
     * Calculate tfidf value of phases
     *
     * @param phasePostingItemLst
     * @param fIdxItemGetter
     * @return
     */
    private static List<Pair<Long, Double>> calPhaseTfidf(List<PhasePostingItem> phasePostingItemLst,
                                                          ItemGetters.FIdxItemGetter fIdxItemGetter) {
        List<Pair<Long, Double>> rVal = new ArrayList<Pair<Long, Double>>();
        int df = phasePostingItemLst.size();
        for (PhasePostingItem item : phasePostingItemLst) {
            //int tfMax = fIdxItemGetter.getItem(idx, item.docId).tfMax;
            double tf = (double) item.freq;
            double idf = Math.log(idx.getDocMap().getNumOfDoc() / (double) df) / Math.log(2);
            rVal.add(new Pair<Long, Double>(item.docId, tf * idf));
        }
        return rVal;
    }

    /**
     * The function to find all the phases in a document
     * Algorithm:
     * Maintains a map with dictionary {docId, {position, count}}
     * I don't know how to express the algorithm.....
     *
     * @param postAdaptedItemLst inner list corresponding to doc of a term
     * @return a list of items containing count of the phase in each doc.
     */
    private static List<PhasePostingItem> findPhasePostingItems(
            List<List<PostingItemAdapter>> postAdaptedItemLst) {
        if (postAdaptedItemLst == null || postAdaptedItemLst.isEmpty()) {
            return null;
        }
        int termNum = postAdaptedItemLst.size();
        List<PostingItemAdapter> base = postAdaptedItemLst.get(0);
        // {docId, {position, count}}, ugly, tell me if you have better method.
        HashMap<Long, HashMap<Integer, Integer>> docPosCntDict =
                new HashMap<Long, HashMap<Integer, Integer>>();
        for (PostingItemAdapter item : base) {
            long docId = item.docId;
            if (!docPosCntDict.containsKey(docId)) {
                docPosCntDict.put(docId, new HashMap<Integer, Integer>());
            }
            for (int position : item.postingItem.positions) {
                docPosCntDict.get(docId).put(position, 1);
            }
        }
        for (int i = 1; i < postAdaptedItemLst.size(); i++) {
            List<PostingItemAdapter> adaptedItems = postAdaptedItemLst.get(i);
            for (PostingItemAdapter adaptedItem : adaptedItems) {
                long docId = adaptedItem.docId;
                PostingItem postingItem = adaptedItem.postingItem;
                for (Integer pos : postingItem.positions) {
                    if (docPosCntDict.containsKey(docId)
                            && docPosCntDict.get(docId).containsKey(pos - i)) { //short circuit
                        Integer curCnt = docPosCntDict.get(docId).get(pos - i);
                        docPosCntDict.get(docId).put(pos - i, curCnt + 1); //override the old count
                    }
                }
            }
        }
        Iterator<Map.Entry<Long, HashMap<Integer, Integer>>> docPosCntIter =
                docPosCntDict.entrySet().iterator();
        List<PhasePostingItem> phaseItemsLst = new ArrayList<PhasePostingItem>();
        while (docPosCntIter.hasNext()) {
            Map.Entry<Long, HashMap<Integer, Integer>> docPosCntEntry = docPosCntIter.next();
            HashMap<Integer, Integer> posCntDict = docPosCntEntry.getValue();
            long docId = docPosCntEntry.getKey();
            PhasePostingItem phasePostingItem = new PhasePostingItem();
            phasePostingItem.docId = docId;
            phasePostingItem.freq = 0;
            Iterator<Map.Entry<Integer, Integer>> posCntIter = posCntDict.entrySet().iterator();
            boolean isValid = false;
            while (posCntIter.hasNext()) {
                Map.Entry<Integer, Integer> posCntEntry = posCntIter.next();
                if (posCntEntry.getValue() == termNum) {
                    isValid = true;
                    phasePostingItem.freq++;
                }
            }
            if(isValid) {
                phaseItemsLst.add(phasePostingItem);
            }
        }

        return phaseItemsLst;

    }

    public static class PhasePostingItem {
        public long docId;
        public int freq;

        @Override
        public String toString() {
            return "docId" + docId + " freq:" + freq;
        }
    }

    public static class PostingItemAdapter {
        public PostingItemAdapter() {
            docId = -1;
            postingItem = null;
        }

        public PostingItemAdapter(PostingItem item, long docId) {
            this.docId = docId;
            this.postingItem = item;
        }

        public long docId;
        public PostingItem postingItem;

        @Override
        public String toString() {
            return "docId:" + docId + postingItem.toString();
        }
    }
}
