package hk.ust.ggsearch.query;

import hk.ust.ggsearch.domain.ForwardIndexItem;
import hk.ust.ggsearch.domain.PostingItem;
import hk.ust.ggsearch.index.Index;
import org.mapdb.Fun;

/**
 * Helpers for more generic functions
 */
public class ItemGetters {
    public static interface IIdxItemGetter {
        public PostingItem getItem(Index idx, Fun.Tuple2<Long, Long> key);
    }

    public static interface FIdxItemGetter {
        public ForwardIndexItem getItem(Index idx, long key);
    }

    public static class BodyIIdxItemGetter implements IIdxItemGetter {
        public PostingItem getItem(Index idx, Fun.Tuple2<Long, Long> key) {
            return idx.getInvertedIndices().getBodyPostingItem(key.a, key.b);
        }
    }

    public static class TitleIIdxItemGetter implements IIdxItemGetter {
        public PostingItem getItem(Index idx, Fun.Tuple2<Long, Long> key) {
            return idx.getInvertedIndices().getTitlePostingItem(key.a, key.b);
        }
    }

    public static class BodyFIdxItemGetter implements FIdxItemGetter {
        public ForwardIndexItem getItem(Index idx, long key) {
            return idx.getForwardIndices().getBodyForwardIndexItem(key);
        }
    }

    public static class TitleFIdxItemGetter implements FIdxItemGetter {
        public ForwardIndexItem getItem(Index idx, long key) {
            return idx.getForwardIndices().getTitleForwardIndexItem(key);
        }
    }
}
