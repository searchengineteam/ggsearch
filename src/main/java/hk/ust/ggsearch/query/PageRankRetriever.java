package hk.ust.ggsearch.query;


import hk.ust.ggsearch.index.Index;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PageRankRetriever {
    public static final double threshold = 0.000001;

    public static Map<Long, Double> getSortedPageRnkTable(List<WeightedToken> terms) {
        Map<Long, Double> simRes = SimilarityCalculator.getSortedCosineSimilarityTable(terms);

        List<Long> docList = new ArrayList<Long>();
        for (Map.Entry<Long, Double> entry : simRes.entrySet()) {
            //System.out.println("" + entry.getValue());
            if(entry.getValue() < threshold) {
                continue;
            }
            docList.add(entry.getKey());
        }
        Index idx = Index.getInstance();
        Map<Long, Double> unsortedPageRankTable = new HashMap<Long, Double>();
        for (int i = 0; i < docList.size(); i++) {
            long docId = docList.get(i);
            double pageRankScore = idx.getCitationIndex().getPageRank(docId);
            unsortedPageRankTable.put(docId, pageRankScore);
        }

        Sorter sorter = new Sorter();
        Map<Long, Double> sortedPageRankTable = sorter.sortByValue(unsortedPageRankTable);
        return sortedPageRankTable;
    }
}