package hk.ust.ggsearch.query;

public class WeightedToken {

    String term;
    double weight;
    int position;

    public WeightedToken(String term) {
        //a very default single term
        this.term = term;
        this.weight = 1.0;
        this.position = -1;
    }

    public WeightedToken(String term, double weight, int position) {
        //position -1 means single term, otherwise part of a phrase
        this.term = term;
        this.weight = weight;
        this.position = position;
    }

    public boolean isInPhase() {
        return position >= 0;
    }

    @Override
    public String toString() {
        return String.format("T:%s, W:%f, P:%d", term, weight, position);
    }
}
