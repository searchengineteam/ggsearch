package hk.ust.ggsearch.query;


import hk.ust.ggsearch.utils.StopStem;

import java.util.ArrayList;
import java.util.List;

public class QueryParser {
    String queryStringProcessing = null;
    List<WeightedToken> queryListProcessing; //query in form of List
    StopStem stopStem;

    /**
     * Constructor
     */
    public QueryParser() {
        //Stem and stop words
        stopStem = new StopStem("stopwords.txt");
    }

    /**
     * absorb a string as queryStringProcessing
     */
    private void absorb(String query) {
        this.queryStringProcessing = query;
    }

    /**
     * parse the queryStringProcessing to queryListProcessing
     */
    private void parse() {
        this.queryListProcessing = new ArrayList<WeightedToken>();

        if (queryStringProcessing == null) {
            System.out.println("No query to process!");
            return;
        }

        //Split the query string
        String[] subQueries = queryStringProcessing.split("\"");

        int sizeOfSubQueries = subQueries.length;

        //Push query terms into queryArray one by one
        if (sizeOfSubQueries % 2 == 1) {//single double-quote(s)
            for (int i = 0; i < sizeOfSubQueries; i++) {
                if ((i % 2 == 1 || i == sizeOfSubQueries - 1))
                    iTermsProcessing(subQueries[i], i);
                else
                    pTermsProcessing(subQueries[i]);
            }
        } else {//pair-up double-quotes, there exist(s) phrase(s)
            for (int i = 0; i < sizeOfSubQueries; i++) {
                if (i % 2 == 0)
                    iTermsProcessing(subQueries[i], i);
                else
                    pTermsProcessing(subQueries[i]);
            }
        }
    }

    /**
     * Process Individual Terms
     */
    private void iTermsProcessing(String iTermString, int index) {
        if (!iTermString.matches("\\s+|^$")) {//individual term
            iTermString = iTermString.replaceAll("[\\W&&[^:.\\-]]+", " ");
            String[] iTerms = iTermString.split("\\s");

            for (int i = 0; i < iTerms.length; i++) {
                String iTerm = iTerms[i];
                String[] splittediTerms = iTerm.split(":");
                String term = splittediTerms[0];
                double weight = 1;
                if (splittediTerms.length > 1) {
                    if (isDouble(splittediTerms[1])) {
                        weight = Double.parseDouble(splittediTerms[1]);
                    }
                }
                if (term.equals("")) {
                    if (index != 0 && index % 2 == 0 && i == 0) {
                        queryListProcessing.get(queryListProcessing.size() - 1).weight = weight;
                    }
                    continue;
                }
                if (!stopStem.isStopWord(term)) {
                    WeightedToken weightedToken = new WeightedToken(stopStem.stem(term), weight, -1);
                    queryListProcessing.add(weightedToken);
                }
            }
        }
    }

    /**
     * Process Phrase Terms
     */
    private void pTermsProcessing(String pTermString) {
        if (!pTermString.matches("\\s+|^$")) {//phrase
            pTermString = pTermString.replaceAll("\\W+", " ");
            String truncatedpTermString = pTermString.trim();
            String[] pTerms = truncatedpTermString.split("\\s");

            int sizeOfPhrase = pTerms.length;

            for (int j = 0; j < sizeOfPhrase; j++) {
                if (!stopStem.isStopWord(pTerms[j])) {
                    WeightedToken weightedToken = new WeightedToken(stopStem.stem(pTerms[j]), 1, j);
                    this.queryListProcessing.add(weightedToken);
                }
            }
        }
    }

    /**
     * absorb and parse and return the token list
     */
    public List<WeightedToken> process(String query) {
        absorb(query);
        parse();
        return queryListProcessing;
    }

    /**
     * a tiny function to check whether a string is in the format of a double
     */
    private boolean isDouble(String str) {
        try {
            Double.parseDouble(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}

