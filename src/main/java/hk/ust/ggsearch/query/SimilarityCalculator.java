package hk.ust.ggsearch.query;

import hk.ust.ggsearch.index.Index;
import hk.ust.ggsearch.utils.Pair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SimilarityCalculator {
    private static final double titleWeight = 0.6;
    private static final double bodyWeight = 1 - titleWeight;

    public static Map<Long, Double> getSortedInnerProductSimilarityTable(List<WeightedToken> terms) {
        Map<Long, Pair<Double, Double>> unsortedInnerProductSimilarityTable = new HashMap<Long, Pair<Double, Double>>();

        double queryL2Norm = 0;

        for (int i = 0; i < terms.size(); i++) {
            if (terms.get(i).position == -1) {//single word
                List<Pair<Long, Pair<Double, Double>>> partialSocores =
                        PartialScoreCalculator.getPartialScoreOfTerm(terms.get(i).term);
                addScore(partialSocores, terms.get(i).weight, unsortedInnerProductSimilarityTable);
            } else if (terms.get(i).position == 0) {//spot phrase
                List<WeightedToken> phrase = new ArrayList<WeightedToken>();
                while (i < terms.size() && terms.get(i).position != -1) {
                    phrase.add(terms.get(i));
                    i++;
                }
                List<Pair<Long, Pair<Double, Double>>> partialScores = PartialScoreCalculator.getPartialScoresOfPhase(phrase);
                addScore(partialScores, terms.get(i - 1).weight, unsortedInnerProductSimilarityTable);
            }
        }

        Map<Long, Double> unsortedInnerProductSimilarityTableIntegrated =
                integrateTitleBodyScore(unsortedInnerProductSimilarityTable);

        Sorter sorter = new Sorter();
        Map<Long, Double> sortedInnerProductSimilarityTable = sorter.sortByValue(unsortedInnerProductSimilarityTableIntegrated);
        return sortedInnerProductSimilarityTable;
    }

    public static Map<Long, Double> getSortedCosineSimilarityTable(List<WeightedToken> terms) {
        Map<Long, Pair<Double, Double>> unsortedCosineSimilarityTable = new HashMap<Long, Pair<Double, Double>>();

        double queryL2Norm = 0;

        for (int i = 0; i < terms.size(); i++) {
            if (terms.get(i).position == -1) {//single word
                List<Pair<Long, Pair<Double, Double>>> partialScores =
                        PartialScoreCalculator.getPartialScoreOfTerm(terms.get(i).term);
                addScore(partialScores, terms.get(i).weight, unsortedCosineSimilarityTable);
                queryL2Norm += Math.pow(terms.get(i).weight, 2);
            } else if (terms.get(i).position == 0) {//spot phrase
                List<WeightedToken> phrase = new ArrayList<WeightedToken>();
                while (i < terms.size() && terms.get(i).position != -1) {
                    phrase.add(terms.get(i));
                    i++;
                }
                List<Pair<Long, Pair<Double, Double>>> partialSocores =
                        PartialScoreCalculator.getPartialScoresOfPhase(phrase);
                addScore(partialSocores, terms.get(i - 1).weight, unsortedCosineSimilarityTable);
                queryL2Norm += Math.pow(terms.get(i - 1).weight, 2);
            }
        }

        queryL2Norm = Math.sqrt(queryL2Norm);

        Index idx = Index.getInstance();

        for (long docId : unsortedCosineSimilarityTable.keySet()) {
            double titleL2Norm = idx.getForwardIndices().getTitleForwardIndexItem(docId).l2Norm;
            double bodyL2Norm = idx.getForwardIndices().getBodyForwardIndexItem(docId).l2Norm;
            Pair<Double, Double> oldPartialScore = unsortedCosineSimilarityTable.get(docId);
            double oldTitleScore = oldPartialScore.first();
            double oldBodyScore = oldPartialScore.second();
            unsortedCosineSimilarityTable.put(docId, new Pair<Double, Double>(oldTitleScore / titleL2Norm / queryL2Norm,
                    oldBodyScore / bodyL2Norm / queryL2Norm));
        }

        Map<Long, Double> unsortedCosineSimilarityTableIntegrated =
                integrateTitleBodyScore(unsortedCosineSimilarityTable);
        Sorter sorter = new Sorter();
        Map<Long, Double> sortedSimilarityTable = sorter.sortByValue(unsortedCosineSimilarityTableIntegrated);

        return sortedSimilarityTable;
    }

    private static void addScore(List<Pair<Long, Pair<Double, Double>>> docs,
                                 double weight, Map<Long, Pair<Double, Double>> docMap) {
        System.out.println("weight:" + weight);
        for (Pair<Long, Pair<Double, Double>> doc : docs) {
            long docId = doc.first();
            Pair<Double, Double> partialScore = doc.second();
            double titleScore = partialScore.first();
            double bodyScore = partialScore.second();

            if (docMap.containsKey(docId)) {
                Pair<Double, Double> oldPartialScore = docMap.get(docId);
                double oldTitleScore = oldPartialScore.first();
                double oldBodyScore = oldPartialScore.second();
                //System.out.println("old" + oldBodyScore);
                docMap.put(docId, new Pair<Double, Double>(oldTitleScore + titleScore * weight,
                        oldBodyScore + bodyScore * weight));
                //System.out.println(bodyScore * weight);
            } else {
                docMap.put(docId, new Pair<Double, Double>(titleScore * weight, bodyScore * weight));
            }
        }
    }

    private static Map<Long, Double> integrateTitleBodyScore(Map<Long, Pair<Double, Double>> separatedMap) {
        Map<Long, Double> integratedMap = new HashMap<Long, Double>();
        for (long docId : separatedMap.keySet()) {
            Pair<Double, Double> separeatedScores = separatedMap.get(docId);
            integratedMap.put(docId, separeatedScores.first() * titleWeight + separeatedScores.second() * bodyWeight);
        }
        return integratedMap;
    }
}

