package hk.ust.ggsearch.utils;

/**
 * Created by gary_li on 11/19/13.
 */
public class Pair<T1, T2> {
    private T1 o1;
    private T2 o2;

    public Pair(T1 o1, T2 o2) {
        this.o1 = o1;
        this.o2 = o2;
    }

    public T1 first() {
        return o1;
    }

    public T2 second() {
        return o2;
    }

    @Override
    public String toString() {
        return "a:" + o1 + " b:" + o2;
    }
}
