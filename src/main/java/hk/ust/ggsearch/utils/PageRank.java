package hk.ust.ggsearch.utils;

import java.util.HashMap;
import java.util.NavigableSet;
import java.util.SortedSet;
import java.util.TreeSet;

import org.mapdb.Fun;

public class PageRank {
    private NavigableSet<Fun.Tuple2<Long, Long>> citationIndex = null;
    private HashMap<Long, Double> pageRankInfoIndex = null;
    private HashMap<Long, Long> precalculatedNumOfChildren = null;
    
    public PageRank(NavigableSet<Fun.Tuple2<Long, Long>> citationIndex,
            HashMap<Long, Double> pageRankInfoIndex) {
        this.citationIndex = new TreeSet<Fun.Tuple2<Long, Long>>(citationIndex);
        this.pageRankInfoIndex = new HashMap<Long, Double>(pageRankInfoIndex);
        this.precalculatedNumOfChildren = new HashMap<Long, Long>();
        closeNetworkAndPrecalculation();
    }
    
    public HashMap<Long, Double> getPageRankInfoIndex() {
        return pageRankInfoIndex;
    }
    
    /**
     * close the network (zero export)
     * pre-calculate the number of children of each parent
     *
     */
    private void closeNetworkAndPrecalculation() {
        NavigableSet<Fun.Tuple2<Long, Long>> newCitationIndex = new 
                TreeSet<Fun.Tuple2<Long, Long>>();
        for (long parent : pageRankInfoIndex.keySet()) {
            SortedSet<Fun.Tuple2<Long, Long>> subCitationIndex = 
                    citationIndex.subSet(Fun.t2(parent, Long.MIN_VALUE), Fun.t2(parent, Long.MAX_VALUE));
            long numOfChildren = 0;
            for (Fun.Tuple2<Long, Long> t : subCitationIndex) {
                if (pageRankInfoIndex.keySet().contains(t.b)) {
                    newCitationIndex.add(t);
                    numOfChildren++;
                }
            }
            precalculatedNumOfChildren.put(parent, numOfChildren);
        }
        citationIndex = newCitationIndex;
    }

    /**
     * run iterations of page rank
     * assume no duplication of children
     * assume the network is cleaned
     *
     * @param numOfIteration
     * @param d the damping value
     * 
     * @return true if the algorithm has converged
     */
    public boolean pageRankGivenNumIteration(int numOfIteration, double d) {
        HashMap<Long, Double> oldPageRankInfoIndex = null;
        for (int i = 0; i < numOfIteration; i++) {
            oldPageRankInfoIndex = pageRankInfoIndex;
            pageRankBase(d);
        }
        if (pageRankInfoIndex.equals(oldPageRankInfoIndex)) {
            return true;
        }
        return false;
    }

    /**
     * one step of page rank
     * assume no duplication of children
     * assume the network is closed
     *
     * @param d the damping value
     */
    private void pageRankBase(double d) {
        HashMap<Long, Double> newPageRankInfoIndex = new HashMap<Long, Double>();
        for (long key : pageRankInfoIndex.keySet()) {
            newPageRankInfoIndex.put(key, 1 - d);
        }
        for (Fun.Tuple2<Long, Long> t : citationIndex) {
            Double parentScore = pageRankInfoIndex.get(t.a);
            if (parentScore == null) {
                System.out.println("Err?");
            }
            long numOfChildren = precalculatedNumOfChildren.get(t.a);
            double oldScore = newPageRankInfoIndex.get(t.b);
            newPageRankInfoIndex.put(t.b, d * parentScore / numOfChildren + oldScore);
        }
        pageRankInfoIndex = newPageRankInfoIndex;
    }
}
