package hk.ust.ggsearch.utils;

import java.io.*;
import java.util.HashSet;

public class StopStem {
    private PorterStemmer porter;
    private HashSet<String> stopWords;

    //Constructor for test
    public StopStem() {
        porter = new PorterStemmer();
        stopWords = new HashSet<String>();

        stopWords.add("is");
        stopWords.add("am");
        stopWords.add("are");
        stopWords.add("was");
        stopWords.add("were");
    }

    //Constructor using input from a file
    public StopStem(String filename) {
        porter = new PorterStemmer();
        stopWords = new HashSet<String>();

        try {
            FileInputStream fstream = new FileInputStream(filename);
            DataInputStream in = new DataInputStream(fstream);
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            String strLine;
            while ((strLine = br.readLine()) != null) {
                stopWords.add(strLine);
            }
            in.close();
        } catch (Exception e) {
            System.err.println("Error: " + e.getMessage());
        }
    }

    //Check whether a word is a stop word
    public boolean isStopWord(String str) {
        return stopWords.contains(str);
    }

    //Return the stemmed word of the input
    public String stem(String str) {
        return porter.stripAffixes(str);
    }

    //Main function for testing StopStem.java
    public static void main(String[] arg) {
        StopStem stopStem = new StopStem("stopwords.txt");
        String input = "";
        try {
            do {
                System.out.print("Please enter a single English word: ");
                BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
                input = in.readLine();
                if (input.length() > 0) {
                    if (stopStem.isStopWord(input))
                        System.out.println("It should be stopped");
                    else
                        System.out.println("The stem of it is \"" + stopStem.stem(input) + "\"");
                }
            }
            while (input.length() > 0);
        } catch (IOException ioe) {
            System.err.println(ioe.toString());
        }
    }
}