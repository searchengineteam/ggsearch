package hk.ust.ggsearch.index;

import hk.ust.ggsearch.domain.ForwardIndexItem;
import hk.ust.ggsearch.domain.GGSerializer;

import org.mapdb.DB;
import org.mapdb.HTreeMap;

/**
 * Created by gary_li on 11/18/13.
 * This class manage Title Forward index and Body Forward Index
 */
public class ForwardIndices {
    private HTreeMap<Long, ForwardIndexItem> bodyForwardIndex = null;
    private HTreeMap<Long, ForwardIndexItem> titleForwardIndex = null;

    private static ForwardIndices mInstance = new ForwardIndices();
    private DB db = IndexConfig.getDB();

    static ForwardIndices getInstance() {
        return mInstance;
    }

    private ForwardIndices() {
        if (!db.exists(IndexConfig.titleForwardIndexName)) {
            db.createHashMap(IndexConfig.titleForwardIndexName)
                    .valueSerializer(GGSerializer.forwardIndexSerializer)
                    .make();
        }
        if (!db.exists(IndexConfig.bodyForwardIndexName)) {
            db.createHashMap(IndexConfig.bodyForwardIndexName)
                    .valueSerializer(GGSerializer.forwardIndexSerializer)
                    .make();
        }

        bodyForwardIndex = db.getHashMap(IndexConfig.bodyForwardIndexName);
        titleForwardIndex = db.getHashMap(IndexConfig.titleForwardIndexName);

    }

    /**
     * Insert ForwardIndexItems of a doc into ForwardIndexes
     *
     * @param docId
     * @param titleForwardIndexItem
     * @param bodyForwardIndexItem
     */
    void insertForwardIndexItem(long docId,
                                ForwardIndexItem titleForwardIndexItem,
                                ForwardIndexItem bodyForwardIndexItem) {
        titleForwardIndex.put(docId, titleForwardIndexItem);
        bodyForwardIndex.put(docId, bodyForwardIndexItem);
        db.commit();
    }

    /**
     * get a ForwardIndexItem of a doc from TitleForwardIndex
     *
     * @param docId
     */
    public ForwardIndexItem getTitleForwardIndexItem(Long docId) {
        return titleForwardIndex.get(docId);
    }

    /**
     * get a ForwardIndexItem of a doc from BodyForwardIndex
     *
     * @param docId
     */
    public ForwardIndexItem getBodyForwardIndexItem(Long docId) {
        return bodyForwardIndex.get(docId);
    }

    /**
     * remove ForwardIndexItems of a doc from titleForwardIndexes
     *
     * @param docId
     */
    ForwardIndexItem removeTitleForwardIndexItem(long docId) {
        ForwardIndexItem tfii = titleForwardIndex.remove(docId);
        db.commit();
        return tfii;
    }

    /**
     * remove ForwardIndexItems of a doc from bodyForwardIndexes
     *
     * @param docId
     */
    ForwardIndexItem removeBodyForwardIndexItem(long docId) {
        ForwardIndexItem bfii = bodyForwardIndex.remove(docId);
        db.commit();
        return bfii;
    }
}
