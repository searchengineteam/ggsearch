package hk.ust.ggsearch.index;

import hk.ust.ggsearch.domain.GGSerializer;
import hk.ust.ggsearch.domain.PostingItem;

import java.util.HashMap;
import java.util.List;
import java.util.NavigableSet;

import org.mapdb.BTreeKeySerializer;
import org.mapdb.BTreeMap;
import org.mapdb.DB;
import org.mapdb.Fun;

/**
 * Manage Body InvertedIndex and TitleInvertedIndex
 */
public class InvertedIndices {
    private static InvertedIndices mInstance = new InvertedIndices();
    private DB db = IndexConfig.getDB();
    private BTreeMap<Fun.Tuple2<Long, Long>, PostingItem> bodyInvertedIndex = null;
    private BTreeMap<Fun.Tuple2<Long, Long>, PostingItem> titleInvertedIndex = null;

    static InvertedIndices getInstance() {
        return mInstance;
    }

    private InvertedIndices() {
        if (!db.exists(IndexConfig.bodyInvertedIndexName)) {
            db.createTreeMap(IndexConfig.bodyInvertedIndexName)
                    .keySerializer(BTreeKeySerializer.TUPLE2)
                    .valueSerializer(GGSerializer.postingItemSerializer)
                    .make();
        }
        if (!db.exists(IndexConfig.titleInvertedIndexName)) {
            db.createTreeMap(IndexConfig.titleInvertedIndexName)
                    .keySerializer(BTreeKeySerializer.TUPLE2)
                    .valueSerializer(GGSerializer.postingItemSerializer)
                    .make();
        }
        bodyInvertedIndex = db.getTreeMap(IndexConfig.bodyInvertedIndexName);
        titleInvertedIndex = db.getTreeMap(IndexConfig.titleInvertedIndexName);

    }

    /**
     * Get a PostingItem from titleInvertedIndex given the termId, docId pair
     *
     * @param termId
     * @param docId
     * @return the corresponding PostingItem in titleInvertedIndex
     */
    public PostingItem getTitlePostingItem(long termId, long docId) {
        return titleInvertedIndex.get(Fun.t2(termId, docId));
    } 
    
    /**
     * Get a PostingItem from bodyInvertedIndex given the termId, docId pair
     *
     * @param termId
     * @param docId
     * @return the corresponding PostingItem in bodyInvertedIndex
     */
    public PostingItem getBodyPostingItem(long termId, long docId) {
        return bodyInvertedIndex.get(Fun.t2(termId, docId));
    } 
    
    /**
     * Insert a list of PostingItems from a doc into InvertedIndices
     *
     * @param posListsAddTitle a map from termId to the posting item to add for titles
     * @param posListsAddBody  a map from termId to the posting item to add for bodies
     */
    void insertPostingItems(long docId, HashMap<Long, PostingItem> posListsAddTitle,
                            HashMap<Long, PostingItem> posListsAddBody) {
        for (Long termId : posListsAddTitle.keySet()) {
            PostingItem pi = posListsAddTitle.get(termId);
            titleInvertedIndex.put(Fun.t2(termId, docId), pi);
        }
        for (Long termId : posListsAddBody.keySet()) {
            PostingItem pi = posListsAddBody.get(termId);
            bodyInvertedIndex.put(Fun.t2(termId, docId), pi);
        }
        db.commit();
    }

    /**
     * remove a doc from a list of terms in the titleInvertedIndex
     *
     * @param docId
     * @param terms terms of the doc
     */
    void removeTitleInvertedIndexPosItems(long docId, List<Long> terms) {
        for (long term : terms) {
            titleInvertedIndex.remove(Fun.t2(term, docId));
        }
        db.commit();
    }

    /**
     * remove a doc from a list of terms in the bodyInvertedIndex
     *
     * @param docId
     * @param terms terms of the doc
     */
    void removeBodyInvertedIndexPosItems(long docId, List<Long> terms) {
        for (long term : terms) {
            bodyInvertedIndex.remove(Fun.t2(term, docId));
        }
        db.commit();
    }

    /**
     * get the keys (termId and docId pairs) for a certain term's posting list in titleInvertedIndex
     *
     * @param termId
     * @param a set of pairs
     */
    public NavigableSet<Fun.Tuple2<Long,Long>> getTitlePostingItemKeys(long termId) {
        return titleInvertedIndex.subMap(Fun.t2(termId, Long.MIN_VALUE), Fun.t2(termId, Long.MAX_VALUE)).keySet();
    }
    
    /**
     * get the keys (termId and docId pairs) for a certain term's posting list in bodyInvertedIndex
     *
     * @param termId
     * @param a set of pairs
     */
    public NavigableSet<Fun.Tuple2<Long,Long>> getBodyPostingItemKeys(long termId) {
        return bodyInvertedIndex.subMap(Fun.t2(termId, Long.MIN_VALUE), Fun.t2(termId, Long.MAX_VALUE)).keySet();
    }
}
