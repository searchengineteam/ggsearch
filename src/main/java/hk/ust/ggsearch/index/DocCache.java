package hk.ust.ggsearch.index;

import hk.ust.ggsearch.domain.DocCacheIndexItem;
import hk.ust.ggsearch.domain.GGSerializer;

import org.mapdb.DB;
import org.mapdb.HTreeMap;

/**
 * Manage doc cache, a doc cache stores the row info of a Webpage.
 */
public class DocCache {
    private HTreeMap<Long, DocCacheIndexItem> docCacheIndex = null;
    private DB db = IndexConfig.getDB();

    private static DocCache ourInstance = new DocCache();

    static DocCache getInstance() {
        return ourInstance;
    }

    private DocCache() {
        if (!db.exists(IndexConfig.docCacheIndexName)) {
            db.createHashMap(IndexConfig.docCacheIndexName)
                    .valueSerializer(GGSerializer.docCacheIndexSerializer)
                    .make();
        }
        docCacheIndex = db.getHashMap(IndexConfig.docCacheIndexName);
    }
    
    /**
     * Insert an html page into the DocCacheIndex
     *
     * @param docId
     * @param page
     */
    void insertDocCacheIndexItem(long docId, DocCacheIndexItem page) {
        docCacheIndex.put(docId, page);
        db.commit();
    }

    /**
     * Get an html page using the docId, return null if the docId not exists
     *
     * @param docId
     * @return
     */
    public DocCacheIndexItem getDocCacheIndexItem(long docId) {
        if (docCacheIndex.containsKey(docId)) {
            return docCacheIndex.get(docId);
        } else {
            return null;
        }
    }

    /**
     * Remove an doc from the DocCacheIndex
     *
     * @param docId
     */
    void removeDocCacheIndexItem(long docId) {
        docCacheIndex.remove(docId);
        db.commit();
    }
}
