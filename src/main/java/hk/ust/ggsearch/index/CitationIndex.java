package hk.ust.ggsearch.index;

import hk.ust.ggsearch.domain.GGSerializer;
import hk.ust.ggsearch.domain.LinkBasedInfoIndexItem;
import hk.ust.ggsearch.utils.PageRank;

import java.util.HashMap;
import java.util.List;
import java.util.NavigableSet;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import org.mapdb.BTreeKeySerializer;
import org.mapdb.DB;
import org.mapdb.Fun;
import org.mapdb.HTreeMap;

/**
 * Created by gary_li on 11/18/13.
 * <p/>
 * Manage citateionIndex and reversedCitationIndex
 * <p/>
 * citateionIndex:
 * parentDocId -> childDocId
 * <p/>
 * reversedCitation:
 * childDocId -> parentDocId
 */
public class CitationIndex {
    private NavigableSet<Fun.Tuple2<Long, Long>> citationIndex = null;
    private NavigableSet<Fun.Tuple2<Long, Long>> reversedCitationIndex = null;
    private HTreeMap<Long, LinkBasedInfoIndexItem> linkBasedInfoIndex = null;
    private DB db = IndexConfig.getDB();

    private static CitationIndex ourInstance = new CitationIndex();

    static CitationIndex getInstance() {
        return ourInstance;
    }

    private CitationIndex() {
        if (!db.exists(IndexConfig.citationIndexName)) {
            db.createTreeSet(IndexConfig.citationIndexName)
                    .serializer(BTreeKeySerializer.TUPLE2)
                    .make();
        }
        if (!db.exists(IndexConfig.reversedCitationIndexName)) {
            db.createTreeSet(IndexConfig.reversedCitationIndexName)
                    .serializer(BTreeKeySerializer.TUPLE2)
                    .make();
        }
        if (!db.exists(IndexConfig.linkBasedInfoIndexName)) {
            db.createHashMap(IndexConfig.linkBasedInfoIndexName)
                    .valueSerializer(GGSerializer.linkBasedInfoIndexItemSerializer)
                    .make();
        }

        citationIndex = db.getTreeSet(IndexConfig.citationIndexName);
        reversedCitationIndex = db.getTreeSet(IndexConfig.reversedCitationIndexName);
        linkBasedInfoIndex = db.getHashMap(IndexConfig.linkBasedInfoIndexName);
    }

    /**
     * insert a parent-child pair into the CitationIndex and ReversedCitationIndex
     *
     * @param parentDocId
     * @param childDocId
     */
    void insertIntoCitationIndex(long parentDocId, List<Long> childrenDocIds) {
        for (long childDocId : childrenDocIds) {
            citationIndex.add(Fun.t2(parentDocId, childDocId));
            reversedCitationIndex.add(Fun.t2(childDocId, parentDocId));
        }
        linkBasedInfoIndex.put(parentDocId, new LinkBasedInfoIndexItem());
        db.commit();
    }

    /**
     * get children of a parent
     *
     * @param parentDocId
     * @return a set of children
     */
    public TreeSet<Long> getChildren(Long parentDocId) {
        SortedSet<Fun.Tuple2<Long, Long>> subset =
                citationIndex.subSet(Fun.t2(parentDocId, Long.MIN_VALUE), Fun.t2(parentDocId, Long.MAX_VALUE));
        TreeSet<Long> children = new TreeSet<Long>();
        for (Fun.Tuple2<Long, Long> t2 : subset) {
            children.add(t2.b);
        }
        return children;
    }

    /**
     * get parents of a child
     *
     * @param childDocId
     * @return a set of parents
     */
    public TreeSet<Long> getParents(Long childDocId) {
        SortedSet<Fun.Tuple2<Long, Long>> subset =
                reversedCitationIndex.subSet(Fun.t2(childDocId, Long.MIN_VALUE),
                        Fun.t2(childDocId, Long.MAX_VALUE));
        TreeSet<Long> parents = new TreeSet<Long>();
        for (Fun.Tuple2<Long, Long> t2 : subset) {
            parents.add(t2.b);
        }
        return parents;
    }

    /**
     * get the LinkBasedInfoIndexItem of a doc
     *
     * @param docId
     * @return linkBasedInfoIndexItem
     */
    public LinkBasedInfoIndexItem getLinkBasedInfoIndexItem(Long docId) {
        return linkBasedInfoIndex.get(docId);
    }

    /**
     * Remove an doc from the parent list of citation indexes 
     * Re-link all old parent to the new one child
     *
     * @param docId
     */
    void removeAndRelinkCitationIndexItem(long oldDocId, long newDocId) {
        linkBasedInfoIndex.remove(oldDocId);
        SortedSet<Fun.Tuple2<Long, Long>> subset =
                citationIndex.subSet(Fun.t2(oldDocId, Long.MIN_VALUE), Fun.t2(oldDocId, Long.MAX_VALUE));
        citationIndex.removeAll(subset);
        for (Fun.Tuple2<Long, Long> t : subset) {
            reversedCitationIndex.remove(Fun.t2(t.b, t.a));
        }
        subset = reversedCitationIndex.subSet(Fun.t2(oldDocId, Long.MIN_VALUE), Fun.t2(oldDocId, Long.MAX_VALUE));
        for (Fun.Tuple2<Long, Long> t : subset) {
            reversedCitationIndex.remove(Fun.t2(t.a, t.b));
            citationIndex.remove(Fun.t2(t.b, t.a));
            if (newDocId != -1) {
                reversedCitationIndex.add(Fun.t2(newDocId, t.b));
                citationIndex.remove(Fun.t2(t.b, newDocId));
            }
        }
        db.commit();
    }
    
    /**
     * reset page rank
     */
    void resetPageRank() {
        Set<Long> parents = linkBasedInfoIndex.keySet();
        for (Long parent : parents) {
            LinkBasedInfoIndexItem lbii = linkBasedInfoIndex.get(parent);
            lbii.pageRank = 1;
            linkBasedInfoIndex.put(parent, lbii);
        }
        db.commit();
    }

    /**
     * extract the page rank information
     * 
     * @return a map from parentId to pageRank
     */
    private HashMap<Long, Double> getPageRankInfoIndex() {
        HashMap<Long, Double> pageRankInfoIndex = new HashMap<Long, Double>();
        for (long parent : linkBasedInfoIndex.keySet()) {
            pageRankInfoIndex.put(parent, linkBasedInfoIndex.get(parent).pageRank);
        }
        return pageRankInfoIndex;
    }
    
    /**
     * put the page rank information into the linkBasedInfoIndex
     * 
     * @param a map from parentId to pageRank
     */
    private void putPageRankInfoIndex(HashMap<Long, Double> pageRankInfoIndex) {
        for (long parent : pageRankInfoIndex.keySet()) {
            LinkBasedInfoIndexItem lbii = linkBasedInfoIndex.get(parent);
            lbii.pageRank = pageRankInfoIndex.get(parent);
            linkBasedInfoIndex.put(parent, lbii);
        }
        db.commit();
    }
    
    /**
     * run page rank
     *
     * @param numIteration
     * @param d damping value
     */
    void runPageRank(int numOfIteration, double d) {
        PageRank pr = new PageRank(citationIndex, getPageRankInfoIndex());
        boolean convergent = pr.pageRankGivenNumIteration(numOfIteration, d);
        if (convergent) {
            System.out.println("Page rank algorithm is converged.");
        }
        putPageRankInfoIndex(pr.getPageRankInfoIndex());
    }
    
    /**
     * get the page rank of a doc
     *
     * @param docId
     */
    public double getPageRank(long docId) {
        if (linkBasedInfoIndex.containsKey(docId)) {
            return linkBasedInfoIndex.get(docId).pageRank;
        } else {
            return -1;
        }
    }
}
