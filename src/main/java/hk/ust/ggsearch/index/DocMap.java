package hk.ust.ggsearch.index;

import hk.ust.ggsearch.domain.DocInfoIndexItem;
import hk.ust.ggsearch.domain.DocTrashIndexItem;
import hk.ust.ggsearch.domain.GGSerializer;
import org.joda.time.DateTime;
import org.mapdb.Atomic;
import org.mapdb.BTreeMap;
import org.mapdb.DB;
import org.mapdb.HTreeMap;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

/**
 * Mange DocIndices and ReversedDocMap and DocCache.
 * DocIndices of the form:
 * docId -> url
 *
 * ReversedDocMap:
 * url -> docId
 *
 * docInfoIndex:
 * docId -> lastModifiedDate
 */
public class DocMap {
    private BTreeMap<Long, String> docMap = null;
    private BTreeMap<String, Long> reversedDocMap = null;
    private Queue<DocTrashIndexItem> docTrashIndex = null;
    private HTreeMap<Long, DocInfoIndexItem> docInfoIndex = null;
    private Atomic.Long docMapKeyInc = null;
    private Atomic.Long numOfDocs = null;

    private DB db = IndexConfig.getDB();

    private static DocMap mInstance = new DocMap();
    
    static DocMap getInstance() {
        return mInstance;
    }

    public long getNumOfDoc() {
        return numOfDocs.get();
    }

    private DocMap() {
        if (!db.exists(IndexConfig.docMapName)) {
            db.createTreeMap(IndexConfig.docMapName)
                    .make();
        }
        if (!db.exists(IndexConfig.reversedDocMapName)) {
            db.createTreeMap(IndexConfig.reversedDocMapName)
                    .make();
        }
        if (!db.exists(IndexConfig.docTrashIndexName)) {
            db.createQueue(IndexConfig.docTrashIndexName, GGSerializer.docTrashIndexItemSerializer);
        }
        if (!db.exists(IndexConfig.numOfDocsName)) {
            db.createAtomicLong(IndexConfig.numOfDocsName, 0);
        }
        if (!db.exists(IndexConfig.docMapKeyIncName)) {
            db.createAtomicLong(IndexConfig.docMapKeyIncName, 0);
        }

        if (!db.exists(IndexConfig.docInfoIndexName)) {
            db.createHashMap(IndexConfig.docInfoIndexName)
                    .valueSerializer(GGSerializer.docInfoIndexItemSerializer)
                    .make();
        }

        docMap = db.getTreeMap(IndexConfig.docMapName);
        reversedDocMap = db.getTreeMap(IndexConfig.reversedDocMapName);
        docInfoIndex = db.getHashMap(IndexConfig.docInfoIndexName);
        docTrashIndex = db.getQueue(IndexConfig.docTrashIndexName);
        docMapKeyInc = db.getAtomicLong(IndexConfig.docMapKeyIncName);
        numOfDocs = db.getAtomicLong(IndexConfig.numOfDocsName);
    }

    /**
     * Get all the available docIds in the database
     * Available here means that details of the web site are stored
     * Based on the keys in the docInfoIndex
     */
    public List<Long> availableDocIds() {
        return new ArrayList<Long>(docInfoIndex.keySet());
    }
    
    /**
     * Check if a docId is available in the database
     * Available here means that details of the web site are stored
     * Based on the keys in the docInfoIndex
     */
    public boolean isDocIdAvailable(long docId) {
        return docInfoIndex.containsKey(docId);
    }
    
    /**
     * Return the docId and return -1 if url not exists in the DocIndices
     *
     * @param url
     * @return docId
     */
    public long getDocId(String url) {
        if (reversedDocMap.containsKey(url)) {
            Long docId = reversedDocMap.get(url);
            return docId;
        } else {
            return -1;
        }
    }
    
    /**
     * Return the URL given a docId in the docMap
     *
     * @param docId
     * @return url
     */
    public String getURL(Long docId) {
        return docMap.get(docId);
    }

    /**
     * Return the URLs given a list of docIds in the docMap
     *
     * @param docIds
     * @return urls
     */
    public List<String> getURLs(List<Long> docIds) {
        List<String> URLs = new ArrayList<String>();
        for (int i = 0; i < docIds.size(); i++) {
            URLs.add(docMap.get(docIds.get(i)));
        }
        return URLs;
    }

    /**
     * Return the lastModifiedDate of a doc
     *
     * @param docId
     * @return last modified date
     */
    public DateTime getLastModificationDate(long docId) {
        DocInfoIndexItem diii = docInfoIndex.get(docId);
        if (diii == null) {
            return null;
        }
        return diii.lastModifiedDate;
    }

    /**
     * Check whether a doc is updated in the database, false means update needed
     *
     * @param url
     * @param dt the datetime to be compared
     * @return true if the lastModifyDate is after specified dt
     */
    public boolean isDocUpdatedAfter(String url, DateTime dt) {
        if(dt == null) {
            return true;
        }
        long docId = getDocId(url);
        DocInfoIndexItem diii = docInfoIndex.get(docId);
        if (diii != null) {
            DateTime lastModifiedDate = diii.lastModifiedDate;
            if (lastModifiedDate == null) {
                return false;
            }
            return !dt.isAfter(lastModifiedDate);
        }
        return false;
    }

    /**
     * increment numOfDocs by 1
     */
    void incrementNumOfDocs() {
        numOfDocs.getAndIncrement();
        db.commit();
    }
    
    /**
     * decrement numOfDocs by 1
     */
    void decrementNumOfDocs() {
        numOfDocs.getAndDecrement();
        db.commit();
    }
    
    /**
     * Insert a url with its lastModifiedDate
     * Return the url of this url after insertion
     * Note that any previous instance of the same url will be put into trash
     *
     * @param url
     * @param lastModifiedDate
     * @return docId
     */
    long insertUrl(String url, DateTime lastModifiedDate) {
        Long newKey = docMapKeyInc.getAndIncrement();
        if (reversedDocMap.containsKey(url)) {
            long docId = reversedDocMap.get(url);
            boolean enqueueSuccess = docTrashIndex.offer(new DocTrashIndexItem(docId, newKey));
            if (!enqueueSuccess) {
                System.out.println("Trash Full! Rolling back!");
                docMapKeyInc.getAndDecrement();
                return -1;
            }
            docInfoIndex.remove(docId);
        }

        docMap.put(newKey, url);
        reversedDocMap.put(url, newKey);
        docInfoIndex.put(newKey, new DocInfoIndexItem(lastModifiedDate));
        db.commit();
        return newKey;
    }

    /**
     * Return the docIds of a list of urls
     * create new docIds if url not exists in the DocIndices
     * typically for getting the ids of a page's children
     *
     * @param urls list of urls
     * @return a list of docIds
     */
    public List<Long> getDocIdsCreatingNew(List<String> urls) {
        ArrayList<Long> docIds = new ArrayList<Long>();
        for (String url: urls) {
            if (reversedDocMap.containsKey(url)) {
                Long docId = reversedDocMap.get(url);
                docIds.add(docId);
            } else {
                Long newKey = docMapKeyInc.getAndIncrement();
                docMap.put(newKey, url);
                reversedDocMap.put(url, newKey);
                docIds.add(newKey);
            }
        }
        db.commit();
        return docIds;
    }
    
    /**
     * Delete a url from database
     *
     * @param url
     */
    long removeUrl(String url) {
        Long docId = reversedDocMap.remove(url);
        if (!(docId == null)) {
            boolean enqueueSuccess = docTrashIndex.offer(new DocTrashIndexItem(docId, -1));
            if (!enqueueSuccess) {
                System.out.println("Trash Full! Rolling back!");
                reversedDocMap.put(url, docId);
                return -2; //Error
            }
            docInfoIndex.remove(docId);
            db.commit();
        } else {
            docId = -1L;
        }
        return docId;
    }
    
    /**
     * Remove an doc from the DocMap
     *
     * @param docId
     */
    void removeDocMapItem(long docId) {
        docMap.remove(docId);
        db.commit();
    }
    
    /**
     * Get the trash
     *
     */
    Queue<DocTrashIndexItem> getDocTrashIndex() {
        return docTrashIndex;
    }
}
