package hk.ust.ggsearch.index;

import hk.ust.ggsearch.domain.DocCacheIndexItem;
import hk.ust.ggsearch.domain.DocTrashIndexItem;
import hk.ust.ggsearch.domain.ForwardIndexItem;
import hk.ust.ggsearch.domain.HTMLPage;
import hk.ust.ggsearch.domain.PostingItem;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.joda.time.DateTime;
import org.mapdb.DB;


public class Index {
    private DB db = IndexConfig.getDB();
    private static Index mInstance = new Index();
    private CitationIndex citationIndex = CitationIndex.getInstance();
    private DocCache docCache = DocCache.getInstance();
    private DocMap docMap = DocMap.getInstance();
    private ForwardIndices forwardIndices = ForwardIndices.getInstance();
    private InvertedIndices invertedIndices = InvertedIndices.getInstance();
    private TermMap termMap = TermMap.getInstance();

    /**
     * Singleton pattern
     *
     * @return get the database instance
     */
    public static Index getInstance() {
        return mInstance;
    }

    private Index() {
    }

    /**
     * Force the database to close
     */
    public void closeIndex() {
        db.close();
    }

    public TermMap getTermMap() {
        return termMap;
    }
    
    public DocMap getDocMap() {
        return docMap;
    }
    
    public CitationIndex getCitationIndex() {
        return citationIndex;
    }

    public DocCache getDocCache() {
        return docCache;
    }

    public ForwardIndices getForwardIndices() {
        return forwardIndices;
    }

    public InvertedIndices getInvertedIndices() {
        return invertedIndices;
    }

    /**
     * get a map mapping terms to total frequencies
     * i.e. the total appearance time in both title and body
     *
     * @param docId
     * @return a map mapping terms to frequencies
     */
    public Map<Long, Integer> getTermTotalFrequencyMap(long docId) {
        Map<Long, Integer> result = new HashMap<Long, Integer>();
        ForwardIndexItem tfii = forwardIndices.getTitleForwardIndexItem(docId);
        List<Long> tTermIds = tfii.terms;
        for (long termId : tTermIds) {
            PostingItem pi = invertedIndices.getTitlePostingItem(termId, docId);
            result.put(termId, pi.freq);
        }
        ForwardIndexItem bfii = forwardIndices.getBodyForwardIndexItem(docId);
        List<Long> bTermIds = bfii.terms;
        for (long termId : bTermIds) {
            PostingItem pi = invertedIndices.getBodyPostingItem(termId, docId);
            Integer oFreq = result.get(termId);
            if (oFreq == null) {
                oFreq = 0;
            }
            result.put(termId, oFreq + pi.freq);
        }
        return result;
    }
    
    /**
     * Insert the terms of a doc into the corresponding indexes
     *
     * @param docId
     * @param titleTerms
     * @param bodyTerms
     */
    private void insertIntoForwardnInvertedIndices(long docId,  List<String> titleTerms,
                                                   List<String> bodyTerms){
        List<Long> titleTermIds = termMap.getTermIds(titleTerms);
        List<Long> bodyTermIds = termMap.getTermIds(bodyTerms);
        HashMap<Long, PostingItem> titleTermHashMap = new HashMap<Long, PostingItem>();
        int tfMaxTitle = 0;
        for (int i = 0; i < titleTermIds.size(); i++) {
            Long titleTermId = titleTermIds.get(i);
            PostingItem pi = null;
            if (titleTermHashMap.containsKey(titleTermId)) {
                pi = titleTermHashMap.get(titleTermId);
                pi.freq++;
                pi.positions.add(i);
            } else {
                ArrayList<Integer> positions = new ArrayList<Integer>();
                positions.add(i);
                pi = new PostingItem(1, positions);
            }
            tfMaxTitle = (pi.freq > tfMaxTitle)? pi.freq : tfMaxTitle;
            titleTermHashMap.put(titleTermId, pi);
        }
        Collection<Long> titleTermCollection = titleTermHashMap.keySet();
        double tL2Norm = 0;
        for (PostingItem pi : titleTermHashMap.values()) {
            tL2Norm += Math.pow(pi.freq, 2);
        }
        tL2Norm = Math.sqrt(tL2Norm);
        ForwardIndexItem tfii = new ForwardIndexItem(tfMaxTitle, new ArrayList<Long>(titleTermCollection), tL2Norm);
        HashMap<Long, PostingItem> bodyTermHashMap = new HashMap<Long, PostingItem>();
        int tfMaxBody = 0;
        for (int i = 0; i < bodyTermIds.size(); i++) {
            Long bodyTermId = bodyTermIds.get(i);
            PostingItem pi = null;
            if (bodyTermHashMap.containsKey(bodyTermId)) {
                pi = bodyTermHashMap.get(bodyTermId);
                pi.freq++;
                pi.positions.add(i);
            } else {
                ArrayList<Integer> positions = new ArrayList<Integer>();
                positions.add(i);
                pi = new PostingItem(1, positions);
            }
            tfMaxBody = (pi.freq > tfMaxBody)? pi.freq : tfMaxBody;
            bodyTermHashMap.put(bodyTermId, pi);
        }
        Collection<Long> bodyTermCollection = bodyTermHashMap.keySet();
        double bL2Norm = 0;
        for (PostingItem pi : titleTermHashMap.values()) {
            bL2Norm += Math.pow(pi.freq, 2);
        }
        bL2Norm = Math.sqrt(bL2Norm);
        ForwardIndexItem bfii = new ForwardIndexItem(tfMaxBody, new ArrayList<Long>(bodyTermCollection), bL2Norm);
        //termMap.incrementTitleDfs(titleTermCollection);
        //termMap.incrementBodyDfs(bodyTermCollection);
        forwardIndices.insertForwardIndexItem(docId, tfii, bfii);
        invertedIndices.insertPostingItems(docId, titleTermHashMap, bodyTermHashMap);
    }
    
    /**
     * Insert a mURL with the information needed
     *
     * @param page
     */
    public void insert(HTMLPage page) {
        String url = page.url;
        DateTime lastModifiedDate = page.modifiedDate;
        List<String> childrenURL = page.children;
        List<String> newPageTitle = page.title;
        List<String> newPageBody = page.body;
        String htmlPage = page.rawHtml;
        String title = page.rawTitle;
        double pageSize = htmlPage.getBytes().length;

        //long start;
        
        //start = (new DateTime()).getMillis();
        long newDocId = docMap.insertUrl(url, lastModifiedDate);
        if (newDocId == -1) {
            clean();
            newDocId = docMap.removeUrl(url);
            if (newDocId == -1) {
                System.out.println("Fatal Error: Cleaned Trash Full! Cannot delete old information!");
                return;
            }
        }
        //System.out.println("InsertDoc time cost: " + ((new DateTime()).getMillis() - start));
        //start = (new DateTime()).getMillis();
        DocCacheIndexItem html = new DocCacheIndexItem(title, htmlPage, pageSize);
        docCache.insertDocCacheIndexItem(newDocId, html);
        //System.out.println("InsertDocCache time cost: " + ((new DateTime()).getMillis() - start));

        //start = (new DateTime()).getMillis();
        List<Long> childrenDocIds = docMap.getDocIdsCreatingNew(childrenURL);
        citationIndex.insertIntoCitationIndex(newDocId, childrenDocIds);
        //System.out.println("InsertCitation time cost: " + ((new DateTime()).getMillis() - start));

        //start = (new DateTime()).getMillis();
        insertIntoForwardnInvertedIndices(newDocId, newPageTitle, newPageBody);
        //System.out.println("InsertForwardInverted time cost: " + ((new DateTime()).getMillis() - start));
        
        docMap.incrementNumOfDocs();
    }

    /**
     * Clean the database based on the trash
     *
     */
    public void clean() {
        DocTrashIndexItem dtii;
        while ((dtii = docMap.getDocTrashIndex().poll()) != null) {
            docMap.removeDocMapItem(dtii.oldDocId);
            docCache.removeDocCacheIndexItem(dtii.oldDocId);
            citationIndex.removeAndRelinkCitationIndexItem(dtii.oldDocId, dtii.newDocId);
            ForwardIndexItem tfii = forwardIndices.removeTitleForwardIndexItem(dtii.oldDocId);
            ForwardIndexItem bfii = forwardIndices.removeBodyForwardIndexItem(dtii.oldDocId);
            if (tfii != null) {
                invertedIndices.removeTitleInvertedIndexPosItems(dtii.oldDocId, tfii.terms);
                //termMap.decrementTitleDfs(tfii.terms);
            }
            if (bfii != null) {
                invertedIndices.removeBodyInvertedIndexPosItems(dtii.oldDocId, bfii.terms);
                //termMap.decrementBodyDfs(bfii.terms);
            }
            if (tfii != null || bfii != null) {
                docMap.decrementNumOfDocs();
            }
        }
    }

    /**
     * Delete a Doc when errors in the page is found in the new retrieving
     *
     * @param url
     */
    public void dealWithPageError(String url) {
        long docId = docMap.removeUrl(url);
        if (docId == -2) {
            clean();
            docId = docMap.removeUrl(url);
            if (docId == -2) {
                System.out.println("Fatal Error: Cleaned Trash Full! Cannot delete old information!");
                return;
            }
        }
    }

    /**
     * run page rank
     *
     * @param numIteration
     * @param d damping value
     */
    public void runPageRank(int numOfIteration, double d) {
        citationIndex.runPageRank(numOfIteration, d);
    }
    
    /**
     * reset page rank
     */
    public void resetPageRank() {
        citationIndex.resetPageRank();
    }
    
    /**
     * For testing purpose
     */
    @Override
    public String toString() {
        return "";
    }
}
