package hk.ust.ggsearch.index;

import hk.ust.ggsearch.domain.GGSerializer;
import hk.ust.ggsearch.domain.TermInfoIndexItem;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.mapdb.Atomic;
import org.mapdb.BTreeMap;
import org.mapdb.DB;
import org.mapdb.HTreeMap;

/**
 * The class that manage term map and reversed term map
 * TermMap is of the structure:
 * termId -> term
 * <p/>
 * Reversed term map is:
 * term -> termId
 * 
 * termInfoIndex is:
 * termId -> df
 */
public class TermMap {
    private BTreeMap<Long, String> termMap = null;
    private BTreeMap<String, Long> reversedTermMap = null;
    private HTreeMap<Long, TermInfoIndexItem> termInfoIndex = null;
    private Atomic.Long termMapKeyInc = null;

    private static TermMap mInstance = new TermMap();
    private DB db = IndexConfig.getDB();

    static TermMap getInstance() {
        return mInstance;
    }

    private TermMap() {
        if (!db.exists(IndexConfig.termMapName)) {
            db.createTreeMap(IndexConfig.termMapName)
                    .make();
        }
        if (!db.exists(IndexConfig.reversedTermMapName)) {
            db.createTreeMap(IndexConfig.reversedTermMapName)
                    .make();
        }
        if (!db.exists(IndexConfig.termMapKeyIncName)) {
            db.createAtomicLong(IndexConfig.termMapKeyIncName, 0);
        }
        if (!db.exists(IndexConfig.termInfoIndexName)) {
            db.createHashMap(IndexConfig.termInfoIndexName)
                    .valueSerializer(GGSerializer.docInfoIndexItemSerializer)
                    .make();
        }
        termInfoIndex = db.getHashMap(IndexConfig.termInfoIndexName);
        termMap = db.getTreeMap(IndexConfig.termMapName);
        reversedTermMap = db.getTreeMap(IndexConfig.reversedTermMapName);

        termMapKeyInc = db.getAtomicLong(IndexConfig.termMapKeyIncName);
    }

    /**
     * Return the termId and create a new one if term not exists in the TermMap
     *
     * @param text
     * @return termId
     */
    public long getTermId(String text) {
        if (reversedTermMap.containsKey(text)) {
            return reversedTermMap.get(text);
        } else {
            Long newKey = termMapKeyInc.getAndIncrement();
            termMap.put(newKey, text);
            reversedTermMap.put(text, newKey);
            db.commit();
            return newKey;
        }
    }

    /**
     * Return the a list of termIds and create new ones if terms not exist in the TermMap
     *
     * @param texts
     * @return the list of the termIds of the given terms
     */
    public List<Long> getTermIds(List<String> texts) {
        ArrayList<Long> termIds = new ArrayList<Long>();
        for (String text : texts) {
            long termId = 0;
            if (reversedTermMap.containsKey(text)) {
                termId = reversedTermMap.get(text);
            } else {
                Long newKey = termMapKeyInc.getAndIncrement();
                termMap.put(newKey, text);
                reversedTermMap.put(text, newKey);
                termId = newKey;
            }
            termIds.add(termId);
        }
        db.commit();
        return termIds;
    }

    /**
     * Return the a list of terms given their termIds
     *
     * @param termIds
     * @return the list of the terms of the given termIds
     */
    public List<String> getTerms(List<Long> termIds) {
        List<String> terms = new ArrayList<String>();
        for (int i = 0; i < termIds.size(); i++) {
            terms.add(termMap.get(termIds.get(i)));
        }
        return terms;
    }

    /**
     * Delete a TermMapItem from the TermMap
     *
     * @param term the term to be deleted
     */
    private void removeTermMapItem(String term) {
        Long termId = reversedTermMap.remove(term);
        if (!(termId == null)) {
            termMap.remove(termId);
        }
        db.commit();
    }

    /**
     * Delete a a list TermMapItems from the TermMap
     *
     * @param terms the list of terms to be deleted
     */
    private void removeTermMapItems(ArrayList<String> terms) {
        for (int i = 0; i < terms.size(); i++) {
            Long termId = reversedTermMap.remove(terms.get(i));
            if (!(termId == null)) {
                termMap.remove(termId);
            }
        }
        db.commit();
    }
    
    /**
     * increment titleDf of a collection of termIds by 1
     * 
     * @param termId
     */
    void incrementTitleDfs(Collection<Long> termIds) {
        for (long termId : termIds) {
            if (termInfoIndex.containsKey(termId)) {
                TermInfoIndexItem tiii = termInfoIndex.get(termId);
                tiii.titleDf++;
                termInfoIndex.put(termId, tiii);
            }
        }
        db.commit();
    }
    
    /**
     * decrement titleDf of a collection of termIds by 1
     * 
     * @param termId
     */
    void decrementTitleDfs(Collection<Long> termIds) {
        for (long termId : termIds) {
            if (termInfoIndex.containsKey(termId)) {
                TermInfoIndexItem tiii = termInfoIndex.get(termId);
                tiii.titleDf--;
                termInfoIndex.put(termId, tiii);
            }
            db.commit();
        }
    }
    
    /**
     * increment bodyDf of a collection of termIds by 1
     * 
     * @param termId
     */
    void incrementBodyDfs(Collection<Long> termIds) {
        for (long termId : termIds) {
            if (termInfoIndex.containsKey(termId)) {
                TermInfoIndexItem tiii = termInfoIndex.get(termId);
                tiii.bodyDf++;
                termInfoIndex.put(termId, tiii);
            }
            db.commit();
        }
    }
    
    /**
     * decrement bodyDf of a collection of termIds by 1
     * 
     * @param termId
     */
    void decrementBodyDfs(Collection<Long> termIds) {
        for (long termId : termIds) {
            if (termInfoIndex.containsKey(termId)) {
                TermInfoIndexItem tiii = termInfoIndex.get(termId);
                tiii.bodyDf--;
                termInfoIndex.put(termId, tiii);
            }
            db.commit();
        }
    }
}
