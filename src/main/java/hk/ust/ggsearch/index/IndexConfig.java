package hk.ust.ggsearch.index;

import org.mapdb.DB;
import org.mapdb.DBMaker;

import java.io.File;

/**
 * Created by gary_li on 11/18/13.
 */
public class IndexConfig {
    static final String dbName = "ggDB";

    //TermMap: termId -> term (TreeMap)
    static final String termMapName = "TermMap";

    //ReversedTermMap: term -> termId (TreeMap)
    static final String reversedTermMapName = "ReversedTermMap";

    //TermInfoIndex: termId -> df (HashMap)
    static final String termInfoIndexName = "TermInfoIndex";

    //DocIndices: docId -> url (TreeMap)
    static final String docMapName = "DocMap";

    //ReversedDocMap: url -> docId (TreeMap)
    static final String reversedDocMapName = "ReversedDocMap";

    //DocInfoIndex: docId -> lastModifiedDate (HashMap)
    static final String docInfoIndexName = "DocInfoIndex";

    //CitationIndex: parentDocId, childDocId (TreeSet)
    static final String citationIndexName = "CitationIndex";

    //CitationIndex: childDocId, parentDocId (TreeSet)
    static final String reversedCitationIndexName = "ReversedCitationIndex";

    //LinkBasedInfoTable: docId -> pageRank (HashMap)
    static final String linkBasedInfoIndexName = "LinkBasedInfoIndex";

    //BodyForwardIndex: docId -> list<termId>, tfMax (HashMap)
    static final String bodyForwardIndexName = "BodyForwardIndex";
     
    //TitleForwardIndex: docId -> list<termId>, tfMax (HashMap)
    static final String titleForwardIndexName = "TitleForwardIndex";
     
    //BodyInvertedIndex: termId, docId -> freq, list<position> (TreeMap)
    static final String bodyInvertedIndexName = "BodyInvertedIndex";
     
    //TitleInvertedIndex: termId, docId -> freq, list<position> (TreeMap)
    static final String titleInvertedIndexName = "TitleInvertedIndex";
     
    //DocCacheIndex: docId -> rawTitle, rawHtml, pageSize (HashMap)
    static final String docCacheIndexName = "DocCacheIndex";
     
    //DocTableTrash: oldDocId, newDocId (Queue)
    static final String docTrashIndexName = "DocTrashIndex";
     
    //NumOfDocs: docId (Atomic.Long)
    static final String numOfDocsName = "NumOfDocs";
     
    //TermMapKeyInc: termId (Atomic.Long)
    static final String termMapKeyIncName = "TermMapKeyInc";
     
    //DocTableKeyInc: docId (Atomic.Long)
    static final String docMapKeyIncName = "DocTableKeyInc";

    private static DB db = DBMaker.newFileDB(new File(IndexConfig.dbName))
            .closeOnJvmShutdown()
            .make();
    public static DB getDB() {
        return db;
    }
}
