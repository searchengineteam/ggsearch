package hk.ust.ggsearch.exe;

import hk.ust.ggsearch.spider.Spider;

public class SpiderTest {
    public static void main(String[] args) {
        Spider spider = new Spider("http://www.cse.ust.hk/~ericzhao/COMP4321/TestPages/testpage.htm", 30);
        while (spider.hasNext()) {
            System.out.println(spider.nextPage().toString());
        }
    }
}
