package hk.ust.ggsearch.exe;

import hk.ust.ggsearch.domain.DocCacheIndexItem;
import hk.ust.ggsearch.domain.HTMLPage;
import hk.ust.ggsearch.index.Index;
import hk.ust.ggsearch.spider.Spider;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import org.joda.time.DateTime;

public class Phase1 {
    Index idx = Index.getInstance();

    public void runPhase1WithIdx() {
        long start;
        Phase1 p = new Phase1();
        System.out.println("Start Running spider!!!");
        start = (new DateTime()).getMillis();
        p.runSpider();
        System.out.println("Total time cost: " + ((new DateTime()).getMillis() - start));
        System.out.println("Spider done");
        p.dbOutput();
    }
    
    public void runPhase1dot5WithIdx() {
        long start;
        Phase1 p = new Phase1();
        System.out.println("Start Running spider!!!");
        start = (new DateTime()).getMillis();
        p.runSpider();
        System.out.println("Total time cost: " + ((new DateTime()).getMillis() - start));
        System.out.println("Spider done");
        System.out.println("Start Cleaning!!!");
        start = (new DateTime()).getMillis();
        p.idx.clean();
        System.out.println("Total time cost: " + ((new DateTime()).getMillis() - start));
        System.out.println("Start Page Rank!!!");
        start = (new DateTime()).getMillis();
        p.idx.runPageRank(22, 0.85);
        System.out.println("Total time cost: " + ((new DateTime()).getMillis() - start));
        p.dbOutput1dot5();
    }

    public static void main(String[] args) {
        Phase1 p = new Phase1();
        p.runPhase1dot5WithIdx();
    }

    private void runSpider() {
        Spider spider = new Spider("http://www.cse.ust.hk/~ericzhao/COMP4321/TestPages/testpage.htm", 300);
        long start;
        while (spider.hasNext()) {
            HTMLPage curHtmlPage = spider.nextPage();
            if(curHtmlPage == null) {
                //skip the error page
                continue;
            }
            System.out.println("Inserting page");
            System.out.println(curHtmlPage.url);
            start = (new DateTime()).getMillis();
            idx.insert(curHtmlPage);
            System.out.println("Time cost: " + ((new DateTime()).getMillis() - start));
        }
    }

    private void dbOutput() {
        FileOutputStream outstream;
        try {
            outstream = new FileOutputStream("spider_result.txt");
            BufferedWriter out = new BufferedWriter(new OutputStreamWriter(outstream));

            List<Long> docIds = idx.getDocMap().availableDocIds();

            for (int i = 0; i < docIds.size(); i++) {
                System.out.println(String.format("The %d th file", i));
                long docId = docIds.get(i);
                DocCacheIndexItem dcii = idx.getDocCache().getDocCacheIndexItem(docId);
                out.write(dcii.title + "\n");
                out.write(idx.getDocMap().getURL(docId) + "\n");
                out.write(idx.getDocMap().getLastModificationDate(docId).toString() +
                        ", " + dcii.pageSize + "\n");
                
                Map<Long, Integer> freqMap = idx.getTermTotalFrequencyMap(docId);
                List<Long> wordIds = new ArrayList<Long>(freqMap.keySet());
                List<String> words = idx.getTermMap().getTerms(wordIds);
                for (int j = 0; j < wordIds.size(); j++) {
                    out.write(words.get(j) + " " + freqMap.get(wordIds.get(j)) + "; ");
                }
                out.write("\n");
                
                TreeSet<Long> childrenIds = idx.getCitationIndex().getChildren(docId);
                for (long childrenId : childrenIds) {
                    out.write(idx.getDocMap().getURL(childrenId) + "\n");
                }

                out.write("-----------------------------------------" +
                        "--------------------------------------------------" + "\n");
            }

            out.close();
            outstream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    private void dbOutput1dot5() {
        FileOutputStream outstream;
        try {
            outstream = new FileOutputStream("spider_result_1dot5.txt");
            BufferedWriter out = new BufferedWriter(new OutputStreamWriter(outstream));

            List<Long> docIds = idx.getDocMap().availableDocIds();

            for (int i = 0; i < docIds.size(); i++) {
                System.out.println(String.format("The %d th file", i));
                long docId = docIds.get(i);
                DocCacheIndexItem dcii = idx.getDocCache().getDocCacheIndexItem(docId);
                out.write(dcii.title + "\n");
                out.write(idx.getDocMap().getURL(docId) + "\n");
                out.write(idx.getDocMap().getLastModificationDate(docId).toString() +
                        ", " + dcii.pageSize + "\n");
                out.write("Page Rank: " + idx.getCitationIndex().getPageRank(docId) + "\n");
                
                Map<Long, Integer> freqMap = idx.getTermTotalFrequencyMap(docId);
                List<Long> wordIds = new ArrayList<Long>(freqMap.keySet());
                List<String> words = idx.getTermMap().getTerms(wordIds);
                for (int j = 0; j < wordIds.size(); j++) {
                    out.write(words.get(j) + " " + freqMap.get(wordIds.get(j)) + "; ");
                }
                out.write("\n");
                
                TreeSet<Long> childrenIds = idx.getCitationIndex().getChildren(docId);
                for (long childrenId : childrenIds) {
                    out.write(idx.getDocMap().getURL(childrenId) + "\n");
                }

                out.write("-----------------------------------------" +
                        "--------------------------------------------------" + "\n");
            }

            out.close();
            outstream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
