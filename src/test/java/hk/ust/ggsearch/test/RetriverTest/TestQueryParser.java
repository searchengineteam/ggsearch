package hk.ust.ggsearch.test.RetriverTest;

import hk.ust.ggsearch.query.QueryParser;
import hk.ust.ggsearch.query.WeightedToken;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.List;


@RunWith(JUnit4.class)
public class TestQueryParser {
    public static String str1 = "Iran to temporarily halt nuclear program in historic international agreement";
    public static String str2 ="Experience an \"aurora borealis\" in your browser with this stunning panorama";
    public static String str3 = "\"Jonah: A VeggieTales Movie: Bonus Material\"";
    public static String str4 = "A VeggieTales Movie";
    public static String str5 = "\"VeggieTales Movie\"";
    public static String str6 = "try \"VeggieTales Movie\"";
    public static String str7 = "\"Gross Anatomy\"";

    public void caseItemOnly() {
        tst(str1);
    }

    public void caseWithPhase() {
        tst(str2);

    }

    public void simplePhase() {
        tst(str5);
    }

    public void complicatedPhase() {
        tst(str3);
    }

    public void complicatedTerms() {
        tst(str4);

    }

    public void compound() {
        tst(str6);
    }

    @Test
    public void real() {
        tst(str7);
    }


    private void tst(String str) {
        QueryParser parser = new QueryParser();
        List<WeightedToken> tokenLst = parser.process(str);
        for(WeightedToken token: tokenLst) {
            System.out.println(token.toString());
        }

    }

}
