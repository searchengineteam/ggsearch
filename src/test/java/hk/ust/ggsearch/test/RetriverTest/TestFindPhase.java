package hk.ust.ggsearch.test.RetriverTest;

import hk.ust.ggsearch.domain.PostingItem;
import hk.ust.ggsearch.query.PartialScoreCalculator;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

import static junit.framework.Assert.assertTrue;

@RunWith(JUnit4.class)
public class TestFindPhase {
    static Object retrieverObj;
    static Class retrieverCls;
    static Method findPhaseMethod;

    @BeforeClass
    public static void prepare() throws ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchMethodException {

        retrieverCls = Class.forName("hk.ust.ggsearch.query.PartialScoreCalculator");
        retrieverObj = retrieverCls.newInstance();
        findPhaseMethod = retrieverCls.getDeclaredMethod("findPhasePostingItems", new Class[]{List.class});

        findPhaseMethod.setAccessible(true);
    }

    @Test
    public void tstReflection() throws InvocationTargetException, IllegalAccessException {
        List<PartialScoreCalculator.PhasePostingItem> lst =
                (List<PartialScoreCalculator.PhasePostingItem>) findPhaseMethod.invoke(retrieverObj, new Object[]{null});
        assertTrue(lst == null);
    }

    /**
     * This test, single document. All documents with id 1.
     */
    @Test
    public void tst1() throws InvocationTargetException, IllegalAccessException {
        String str = "this is a test of hello word hello haha very good just a test this is haha a test";
        String str2 = "this is another test haha test two do you know that a test two";
        List<String> corpus = new ArrayList<String>();
        corpus.add(str);
        corpus.add(str2);
        List<List<PartialScoreCalculator.PostingItemAdapter>> corpusItems = produceCorpus(corpus, "this is");
        List<PartialScoreCalculator.PhasePostingItem> lst =
                (List<PartialScoreCalculator.PhasePostingItem>) findPhaseMethod.invoke(retrieverObj, new Object[]{corpusItems});
        assertTrue(lst.get(0).freq == 2);
        assertTrue(lst.get(1).freq == 1);

    }

    //@Test
    public void testProduceCorpus() {
        String str = "this is a test of hello word hello haha very good just a test this is haha a test";
        String str2 = "this is another test haha test two do you know that this is a test two";
        List<String> corpus = new ArrayList<String>();
        corpus.add(str);
        corpus.add(str2);
        System.out.println(produceCorpus(corpus, "this is"));
    }

    /**
     * Quick function to convert corpus for testing, no performance concern at all
     *
     * @param corpus
     * @return
     */
    private static List<List<PartialScoreCalculator.PostingItemAdapter>> produceCorpus(List<String> corpus, String phase) {
        HashMap<String, ArrayList<PartialScoreCalculator.PostingItemAdapter>> resultDict =
                new HashMap<String, ArrayList<PartialScoreCalculator.PostingItemAdapter>>();
        int count = 0;
        for (String doc : corpus) {
            HashSet<String> termsFound = new HashSet<String>();
            count++;
            String[] terms = doc.split(" ");
            for (String term : terms) {
                if (!termsFound.contains(term)) {
                    termsFound.add(term);
                    if (!resultDict.containsKey(term)) {
                        resultDict.put(term, new ArrayList<PartialScoreCalculator.PostingItemAdapter>());
                    }
                    resultDict.get(term).add(findTerm(count, term, terms));
                }
            }
        }
        Iterator<Map.Entry<String, ArrayList<PartialScoreCalculator.PostingItemAdapter>>> entries
                = resultDict.entrySet().iterator();
        List<List<PartialScoreCalculator.PostingItemAdapter>> rVal = new ArrayList<List<PartialScoreCalculator.PostingItemAdapter>>();
        String[] phaseTerms = phase.split(" ");
        for (String t : phaseTerms) {
            rVal.add(resultDict.get(t));
        }
        return rVal;

    }

    private static PartialScoreCalculator.PostingItemAdapter findTerm(long docId, String term, String[] terms) {
        PartialScoreCalculator.PostingItemAdapter rVal = new PartialScoreCalculator.PostingItemAdapter(new PostingItem(0, new ArrayList<Integer>()), docId);
        for (int i = 0; i < terms.length; i++) {
            if (terms[i].equals(term)) {
                rVal.postingItem.freq++;
                rVal.postingItem.positions.add(i);
            }
        }
        return rVal;
    }
}
