package hk.ust.ggsearch.test.RetriverTest;

import hk.ust.ggsearch.index.Index;
import hk.ust.ggsearch.query.QueryParser;
import hk.ust.ggsearch.query.SimilarityCalculator;
import hk.ust.ggsearch.query.WeightedToken;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RunWith(JUnit4.class)
public class TestRanking {
    static Index idx =hk.ust.ggsearch.index.Index.getInstance();
    @Test
    public void testCase1() {
        System.out.println("=================================");
        tstSim("movie");
    }

    public void testCase2() {
        System.out.println("====== A VeggieTales Movie========================");
        tstSim("A VeggieTales Movie");
    }

    public void testCase3() {
        System.out.println("====== A VeggieTales Movie (Phase)========================");
        tstSim("\"A VeggieTales Movie\"");
    }

    public void testCase4() {
        System.out.println("=================================");
        tstSim("\"Jonah: A VeggieTales Movie: Bonus Material\"");
    }

    private void tstSim(String str) {
        QueryParser parser = new QueryParser();
        List<WeightedToken> tokenLst = parser.process(str);
        Map<Long, Double> simResult = SimilarityCalculator.getSortedCosineSimilarityTable(tokenLst);
        for (Map.Entry<Long, Double> entry : simResult.entrySet()) {
            System.out.println(entry.getKey().toString() + idx.getDocMap().getURL(entry.getKey())
                    + ":" + entry.getValue().toString());
        }
    }
}
