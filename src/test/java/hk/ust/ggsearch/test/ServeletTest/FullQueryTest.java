package hk.ust.ggsearch.test.ServeletTest;

import hk.ust.ggsearch.query.PageRankRetriever;
import hk.ust.ggsearch.query.QueryParser;
import hk.ust.ggsearch.query.SimilarityCalculator;
import hk.ust.ggsearch.query.WeightedToken;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.List;
import java.util.Map;

/**
 * Created by gary_li on 11/26/13.
 */
@RunWith(JUnit4.class)
public class FullQueryTest {
    @Test
    public void testCosSim() {
        String mQuery = "\"Gross Anatomy\"";
        String mType = "cos";
        QueryParser parser = new QueryParser();
        List<WeightedToken> tokens = parser.process(mQuery);
        for(WeightedToken token:tokens) {
            System.out.println(token.toString());
        }
        Map<Long, Double> retrievedDocs = null;
        if(mType.equals("cos")) {
            System.out.println(mType);
            retrievedDocs = SimilarityCalculator.getSortedCosineSimilarityTable(tokens);
        } else if(mType.equals("prod")) {
            retrievedDocs = SimilarityCalculator.getSortedInnerProductSimilarityTable(tokens);
        } else if(mType.equals("pagerank")) {
            retrievedDocs = PageRankRetriever.getSortedPageRnkTable(tokens);
        } else {
            return;
        }

        for(Map.Entry<Long, Double> entry: retrievedDocs.entrySet()) {
            System.out.println(entry.getKey());
        }


    }
}
